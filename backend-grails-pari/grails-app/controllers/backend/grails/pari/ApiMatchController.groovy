package backend.grails.pari

import javax.servlet.http.HttpServletResponse

class ApiMatchController extends ApiBasicController{

    MatchCustomService matchCustomService
    def matchPlusParie() {
        try {
            switch (request.getMethod()) {
                case "GET":
                    def most = []
                    def jsonObject = request.JSON
                    def max = jsonObject.max ? jsonObject.max.toLong(): 3
                    def offset = jsonObject.offset ? jsonObject.offset.toLong() : 1
                    def result = matchCustomService.matchMostBet(max, offset).each {
                        item ->
                            most.add([count: item[0], idmatch: item[1]])
                    }
                    serializeData(most, request.getHeader("Accept"))
                    break
                default:
                    return response.status = HttpServletResponse.SC_METHOD_NOT_ALLOWED
                    break
            }
        }catch(Exception ex){
            response.status = HttpServletResponse.SC_INTERNAL_SERVER_ERROR
            return  serializeData([message : ex.getMessage()], request.getHeader("Accept"))
        }
    }


}
