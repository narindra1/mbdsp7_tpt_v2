package backend.grails.pari

import backend.grails.marshaller.Marshaller

class BootStrap {

    def init = { servletContext ->
        Marshaller.init()
    }
    def destroy = {
    }
}
