package backend.grails.marshaller

import backend.grails.pari.Paris
import backend.grails.pari.ParisDetail
import backend.grails.pari.Payment
import grails.converters.JSON

import java.text.SimpleDateFormat

class Marshaller {

    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy")
    static init(){
        JSON.registerObjectMarshaller(Paris) { Paris paris ->
            return [
                    'id': paris.id,
                    'datePari': simpleDateFormat.format(paris.datePari),
                    'totalAmount': paris.totalAmount,
                    'isPayed': paris.isPayed,
                    'idUser': paris.idUser
            ]
        }
        JSON.registerObjectMarshaller(Payment) { Payment payment ->
            return [
                    'id': payment.id,
                    'amount': payment.amount,
                    'datePay': simpleDateFormat.format(payment.datePay),
                    'ref': payment.ref,
                    'idUser': payment.idUser,
                    'bankCard': payment.bankCard,
                    'type': payment.type
            ]
        }
    }
    static pariFormat(){
        JSON.registerObjectMarshaller(Paris) { Paris paris ->
            return [
                    'id': paris.id,
                    'datePari': simpleDateFormat.format(paris.datePari),
                    'totalAmount': paris.totalAmount,
                    'isPayed': paris.isPayed,
                    'idUser': paris.idUser,
                    'pariMise': paris.pariMise
            ]
        }
    }
    static parisDetailFormat(){
        JSON.registerObjectMarshaller(ParisDetail) { ParisDetail parisDetail ->
            return [
                    'id'             : parisDetail.id,
                    'amount'         : parisDetail.amount,
                    'idTeamParie'    : parisDetail.idTeamParie,
                    'amountWithQuote': parisDetail.amountWithQuote,
                    'isFinished'     : parisDetail.isFinished,
                    'dateInsert'     : simpleDateFormat.format(parisDetail.dateInsert),
                    'type'           : parisDetail.type,
                    'paris'          : parisDetail.paris,
                    'payment'        : parisDetail.payment
            ]
        }
    }

}
