package backend.grails.pari

import grails.gorm.transactions.Transactional
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject

import java.sql.Date

@Transactional
class PariCustomService {

    ParisService parisService
    ParisDetailService parisDetailService

    def getNotPayed(Serializable id){
        return Paris.createCriteria().list(){
            eq("isPayed", false)
            and {
                eq("idUser", id.toLong())
            }
        }
    }

    def getBetInProgressByIdUser(long iduser){
        return Paris.executeQuery("Select count(pd.id) as number from Paris p join ParisDetail pd on p.id = pd.paris.id where p.idUser =:id and p.isPayed = false", [id: iduser])[0]
    }

    def getBetInProgress(){
        return Paris.executeQuery("Select count(pd.id) as number from Paris p join ParisDetail pd on p.id = pd.paris.id where p.isPayed = false")[0]
    }

    def mostBet(){
        return ParisDetail.executeQuery("select count(idTeamParie), idTeamParie from ParisDetail GROUP BY idTeamParie")
    }

    // à retester
    def udpateStatusIsFinishedByIdMatch(long idmatch){
        return ParisDetail.executeUpdate("UPDATE ParisDetail p set p.isFinished = true where p.idMatch =:idmatch", [idmatch: idmatch]);
    }


    def findAllPariDetailByIdmatch(long idmatch){
        return ParisDetail.findAllByIdMatch(idmatch)
    }

    def insertPari(def jsonObject){

        String iduser = jsonObject.iduser
        String amount = jsonObject.amount
        def lastpari = getBetInProgressByIdUser(iduser.toLong())
        Paris pariInstance
        if(lastpari.toLong() == 0){

            Paris pari = new Paris()
            String datepari = jsonObject.datepari
            println("**************** "+datepari)
            pari.datePari = Date.valueOf(datepari)
            String totalamount = jsonObject.totalamount
            pari.totalAmount = totalamount.toDouble()
            pari.isPayed = jsonObject.ispayed
            pari.idUser = iduser.toLong()
            if (!pari.validate()) {
                throw new Exception('Une erreur de validation est survenue')
            }
            pariInstance = parisService.save(pari)
        }
        else{
            pariInstance = getNotPayed(iduser)[0]

            pariInstance.setTotalAmount(pariInstance.totalAmount+amount.toDouble())
        }

        ParisDetail parisDetail = new ParisDetail()
        println(jsonObject)
        String idmatch = jsonObject.idmatch
        parisDetail.idMatch = idmatch.toLong()
        parisDetail.amount = amount.toDouble()
        String idteamparie = jsonObject.idteamparie
        parisDetail.idTeamParie = idteamparie.toLong()
        String amountwithquote = jsonObject.amountwithquote
        parisDetail.amountWithQuote = amountwithquote.toDouble()
        parisDetail.isFinished = jsonObject.isfinished
        String dateinsert = jsonObject.dateinsert
        parisDetail.dateInsert = Date.valueOf(dateinsert)
        parisDetail.type = jsonObject.type
        Long id = Long.valueOf(pariInstance.id)
        Paris paribyid = parisService.get(id)
        parisDetail.paris = paribyid


        if(!parisDetail.validate()){
            throw new Exception('Une erreur de validation est survenue')
        }
        def parimise = parisDetailService.save(parisDetail)

        return parimise
    }

    def countPariisFinished(boolean status, long max, long offset){
        return [count: ParisDetail.countByIsFinished(status)]
    }

    def insertdetails(def jsonObject){
        List<ParisDetail> paridetails = new ArrayList<>();
        String iduser = jsonObject.iduser
        Paris pari = new Paris()
        String datepari = jsonObject.datepari
        println("**************** "+datepari)
        pari.datePari = Date.valueOf(datepari)
        String totalamount = jsonObject.totalamount
        pari.totalAmount = totalamount.toDouble()
        pari.isPayed = jsonObject.ispayed
        pari.idUser = iduser.toLong()
        if (!pari.validate()) {
            throw new Exception('Une erreur de validation est survenue')
        }
        Paris pariInstance = parisService.save(pari)

        JSONArray jsonArray = jsonObject.pariDetails

        for (int i = 0; i< jsonArray.size(); i++){
            JSONObject jsonDetail = jsonArray.get(i);
            ParisDetail parisDetail = new ParisDetail()
            String idmatch = jsonDetail.idmatch
            String amount = jsonDetail.amount
            parisDetail.idMatch = idmatch.toLong()
            parisDetail.amount = amount.toDouble()
            String idteamparie = jsonDetail.idteamparie
            parisDetail.idTeamParie = idteamparie.toLong()
            String amountwithquote = jsonDetail.amountwithquote
            parisDetail.amountWithQuote = amountwithquote.toDouble()
            parisDetail.isFinished = jsonDetail.isfinished
            String dateinsert = jsonDetail.dateinsert
            parisDetail.dateInsert = Date.valueOf(dateinsert)
            parisDetail.type = jsonDetail.type
            Long id = Long.valueOf(pariInstance.id)
            Paris paribyid = parisService.get(id)
            parisDetail.paris = paribyid

            if(!parisDetail.validate()){
                throw new Exception('Une erreur de validation est survenue')
            }
            def parimise = parisDetailService.save(parisDetail)
            paridetails.add(parimise);
        }

        return paridetails;
    }
}
