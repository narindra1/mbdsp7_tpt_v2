package backend.grails.pari

import grails.gorm.transactions.Transactional

@Transactional
class MatchCustomService {

    def matchMostBet(long max, long offset){
        return ParisDetail.executeQuery("select count(pd.idMatch), pd.idMatch from ParisDetail pd join Paris p on pd.paris.id = p.id where p.isPayed = true GROUP BY pd.idMatch ORDER BY count(pd.idMatch) DESC", [max: max, offset: offset])
    }

}
