import { ElementRef } from '@angular/core';
import { Directive } from '@angular/core';

@Directive({
  selector: '[appCountdown]'
})
export class CountdownDirective {

  constructor(private el: ElementRef) {
    el.nativeElement.style.backgroundColor = 'rgb(134, 143, 143)';
   }

}
