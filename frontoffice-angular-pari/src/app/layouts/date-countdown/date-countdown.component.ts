import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-date-countdown',
  templateUrl: './date-countdown.component.html',
  styleUrls: ['./date-countdown.component.scss']
})
export class DateCountdownComponent implements OnInit {
  @Input() date = ""
  @Input() time = ""

  constructor() { }
  ngOnInit() {
  }

  public toTimestamp(strDate, time){
    let momentVariable = moment(strDate, 'YYYY-MM-DD');
    let datevalue = momentVariable.format('YYYY-MM-DD')
    let datefinal = datevalue+' '+time+':00';
    var datum = Date.parse(datefinal);
    return datum/1000;
  }
}
