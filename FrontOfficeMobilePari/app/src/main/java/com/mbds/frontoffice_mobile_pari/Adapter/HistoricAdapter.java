package com.mbds.frontoffice_mobile_pari.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mbds.frontoffice_mobile_pari.Model.Mouvement;
import com.mbds.frontoffice_mobile_pari.Model.Payment;
import com.mbds.frontoffice_mobile_pari.Model.ResultCustom;
import com.mbds.frontoffice_mobile_pari.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HistoricAdapter extends RecyclerView.Adapter<HistoricAdapter.HistoricViewHolder>{

    public List<ResultCustom> historics;

    public HistoricAdapter(List<ResultCustom> historics){
        this.historics = historics;
    }

    @NonNull
    @Override
    public HistoricAdapter.HistoricViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_result_details, parent, false);
        return new HistoricAdapter.HistoricViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoricAdapter.HistoricViewHolder holder, int position) {
        holder.dateTransaction.setText(historics.get(position).pariDetails.dateInsert);
        holder.match.setText(historics.get(position).match.team_1.name +" - "+ historics.get(position).match.team_2.name);
        holder.finishedScore.setText(historics.get(position).match.score_1 +" - "+historics.get(position).match.score_2);
        holder.debit.setText(String.valueOf(historics.get(position).gain));
        holder.credit.setText(String.valueOf(historics.get(position).perte));
    }

    @Override
    public int getItemCount() {
        return historics.size();
    }

    public class HistoricViewHolder extends RecyclerView.ViewHolder{

        TextView dateTransaction;
        TextView match;
        TextView finishedScore;
        TextView debit;
        TextView credit;

        public HistoricViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            dateTransaction = itemView.findViewById(R.id.dateTrasaction);
            match = itemView.findViewById(R.id.match);
            finishedScore = itemView.findViewById(R.id.finishedScore);
            debit = itemView.findViewById(R.id.debit);
            credit = itemView.findViewById(R.id.credit);
        }
    }
}
