package com.mbds.frontoffice_mobile_pari.Services;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


import com.mbds.frontoffice_mobile_pari.Model.Article;

import java.util.List;

@Dao
public interface ArticleService {
    @Query("SELECT * FROM article")
    List<Article> getAll();

    @Query("SELECT * FROM article ORDER BY id DESC LIMIT 1")
    Article getLast();

    @Insert
    long insert(Article article);
}
