package com.mbds.frontoffice_mobile_pari.Services;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.mbds.frontoffice_mobile_pari.Model.MatchSQLite;

import java.util.List;

@Dao
public interface MatchService {
    @Query("SELECT * FROM MatchSQLite")
    List<MatchSQLite> getAll();

    @Query("SELECT * FROM MatchSQLite ORDER BY date DESC LIMIT 5")
    List<MatchSQLite> getLast();

    @Insert
    long insert(MatchSQLite matchSQLite);
}
