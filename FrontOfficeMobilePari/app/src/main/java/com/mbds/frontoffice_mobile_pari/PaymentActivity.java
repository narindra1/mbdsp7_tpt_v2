package com.mbds.frontoffice_mobile_pari;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mbds.frontoffice_mobile_pari.Model.Pari;
import com.mbds.frontoffice_mobile_pari.Model.PariDetails;
import com.mbds.frontoffice_mobile_pari.Model.PariDetailsSQLite;
import com.mbds.frontoffice_mobile_pari.Model.PariSQLite;
import com.mbds.frontoffice_mobile_pari.R;
import com.mbds.frontoffice_mobile_pari.Services.AppDatabase;
import com.mbds.frontoffice_mobile_pari.Services.GlobalService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaymentActivity extends AppCompatActivity {

    TextView amoutText, connexion_status_payment;
    EditText bank_Card;
    Button Payed_Btn;
    EditText code_bank_card;
    Integer pariIdSqlite = 0;

    GlobalService globalService = new GlobalService();
    AppDatabase database;

    private static final String SERVERPAIMENT = "https://tptnode.herokuapp.com/api/payments";
    private static final String SERVERPARI = "https://tptnode.herokuapp.com/api/pari/details";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        amoutText = findViewById(R.id.amoutText);
        bank_Card = findViewById(R.id.bank_Card);
        Payed_Btn = findViewById(R.id.Payed_Btn);
        code_bank_card = findViewById(R.id.codeBankCard);
        connexion_status_payment = findViewById(R.id.connexion_status_payment);

        boolean isConnected = globalService.isNetworkConnected(this);
        if (!isConnected)
            connexion_status_payment.setVisibility(View.VISIBLE);

        database = AppDatabase.getInstance(this);

        Bundle bundle = getIntent().getExtras();
        /*final String pariID = bundle.getString("pariID");*/
        final String userID = bundle.getString("userID");
        final String reference = bundle.getString("reference");
        final String amount = bundle.getString("amount");
        final Serializable pariSQLite = bundle.getSerializable("pariSQLite");
        final Serializable pariDetailsServices = bundle.getSerializable("pariDetailsServices");
        final PariSQLite pari = (PariSQLite) pariSQLite;
        List<PariDetailsSQLite> paridetails = (List<PariDetailsSQLite>) pariDetailsServices;
        pariIdSqlite = pari.id;

        final JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("iduser", pari.idUser);
            jsonObject.put("datepari", pari.datepari);
            jsonObject.put("totalamount", pari.totalAmount);
            jsonObject.put("ispayed", true);

            JSONArray jsonArray = new JSONArray();
            for (PariDetailsSQLite paridetail: paridetails) {
                JSONObject paridetailJson = new JSONObject();
                paridetailJson.put("idmatch", paridetail.matchid);
                paridetailJson.put("amount", paridetail.amount);
                paridetailJson.put("idteamparie", paridetail.teamparie);
                paridetailJson.put("amountwithquote", paridetail.amountwithquote);
                paridetailJson.put("isfinished", paridetail.isfinished);
                paridetailJson.put("dateinsert", paridetail.date);
                paridetailJson.put("type", paridetail.type);

                jsonArray.put(paridetailJson);
            }
            jsonObject.put("pariDetails", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        amoutText.setText(amount);

        Payed_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bankcard = bank_Card.getText().toString().trim();
                String Codebankcard = code_bank_card.getText().toString().trim();
                if(Codebankcard.length() == 0 || Codebankcard.length() != 4){
                    Toast.makeText(PaymentActivity.this, "Code invalide ! ", Toast.LENGTH_LONG).show();
                }
                else{
                    InsertPari(jsonObject, amount, bankcard, userID, reference, "pari_gagnant_simple");
                }
            }
        });
    }


    public Pari InsertPayment(String amount, String bankcard, String userid, String pariID, String reference, String type){
        final Pari paris = new Pari();
        AndroidNetworking.post(SERVERPAIMENT)
                .addBodyParameter("amount", amount)
                .addBodyParameter("ref", reference)
                .addBodyParameter("iduser", userid)
                .addBodyParameter("bankcard", bankcard)
                .addBodyParameter("idparis", pariID)
                .addBodyParameter("type", type)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i(response.toString(), "onResponse: ");
                        Toast.makeText(PaymentActivity.this, "Paiement effectué avec succès .", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(ANError anError) {
                        return;
                    }
                });
        return paris;
    }

    public void InsertPari(JSONObject jsonObject, final String amount, final String bankcard, final String userid, final String reference, final String type){
        AndroidNetworking.post(SERVERPARI)
                .addJSONObjectBody(jsonObject)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i(response.toString(), "onResponse: ");

                        Toast.makeText(PaymentActivity.this, "Pari validé .", Toast.LENGTH_LONG).show();
                        try {
                            JSONObject explrObject = response.getJSONObject(0);
                            JSONObject paris = explrObject.getJSONObject("paris");
                            String idpari = paris.getString("id");
                            String ref = reference + "" + idpari + "124";
                            InsertPayment(amount, bankcard, userid, idpari, ref, type);
                            database.pariService().updatePariPayed(pariIdSqlite);

                            Intent intent = new Intent(PaymentActivity.this, SummarizeActivity.class);
                            intent.putExtra("bankcard", bankcard);
                            intent.putExtra("reference", ref);
                            intent.putExtra("amount", amount);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        return;
                    }
                });
    }
}