package com.mbds.frontoffice_mobile_pari.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mbds.frontoffice_mobile_pari.Adapter.BagCustomAdapter;
import com.mbds.frontoffice_mobile_pari.Adapter.MatchAdapter;
import com.mbds.frontoffice_mobile_pari.ArticleActivity;
import com.mbds.frontoffice_mobile_pari.CouponActivity;
import com.mbds.frontoffice_mobile_pari.Model.Match;
import com.mbds.frontoffice_mobile_pari.Model.MatchSQLite;
import com.mbds.frontoffice_mobile_pari.Model.PariSQLite;
import com.mbds.frontoffice_mobile_pari.Model.Team;
import com.mbds.frontoffice_mobile_pari.Model.User;
import com.mbds.frontoffice_mobile_pari.Model.UserSQLite;
import com.mbds.frontoffice_mobile_pari.R;
import com.mbds.frontoffice_mobile_pari.Services.AppDatabase;
import com.mbds.frontoffice_mobile_pari.Services.GlobalService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BetFragment extends Fragment {

    String userID = "";

    DatePicker datePicker;
    RecyclerView allMatch;
    int mYear = 0;
    int mMonth = 0;
    int mDay = 0;

    MatchAdapter adapter;
    List<Match> matches;
    String date = "";

    AppDatabase database;
    GlobalService globalService = new GlobalService();
    boolean isConnected = false;

    //    private static final String SERVER = "http://192.168.8.108:8010/api/articles";
    private static final String SERVER = "https://tptnode.herokuapp.com/api/matchs";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = AppDatabase.getInstance(getContext());

        long millis=System.currentTimeMillis();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        date = dateFormat.format(millis);
        Calendar calendar =Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);

        UserSQLite userSQLite = database.userService().findLast();
        userID = userSQLite.userid;

        isConnected = globalService.isNetworkConnected(getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        datePicker = getView().findViewById(R.id.datePicker);
        datePicker.init(mYear, mMonth, mDay, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                String date = year+ "-0" + month +"-"+ dayOfMonth;
                matches = GetNextMatchs(date);
                adapter.matches = matches;
                adapter.notifyDataSetChanged();
            }
        });

        allMatch = getView().findViewById(R.id.allMatch);
        setRecyclerView();

        if (isConnected){
            matches = GetNextMatchs(date);
            adapter.matches = matches;
            adapter.notifyDataSetChanged();
        }

        if (!isConnected){
            List<MatchSQLite> matchSQLites = database.matchService().getLast();
            for (int i = 0; i< matchSQLites.size(); i++) {
                Match match = new Match();
                Team team1 = new Team();
                team1.id = matchSQLites.get(i).team_1_id;
                team1.name = matchSQLites.get(i).team_1_name;
                match.team_1 = team1;

                Team team2 = new Team();
                team2.id = matchSQLites.get(i).team_2_id;
                team2.name = matchSQLites.get(i).team_2_name;
                match.team_2 = team2;

                match.score_1 = matchSQLites.get(i).score_1;
                match.score_2 = matchSQLites.get(i).score_2;
                match.date_time = matchSQLites.get(i).date_time;
                match.date = matchSQLites.get(i).date;
                match.time = matchSQLites.get(i).time;
                match.quote_team1 = matchSQLites.get(i).quote_team1;
                match.quote_team2 = matchSQLites.get(i).quote_team2;
                match.quote_null = matchSQLites.get(i).quote_null;
                match.popularite = matchSQLites.get(i).popularite;
                match.id_win = matchSQLites.get(i).id_win;

                matches.add(match);
            }
            adapter.matches = matches;
            adapter.notifyDataSetChanged();
        }
    }

    private void setRecyclerView(){
        this.matches = new ArrayList<>();
        this.adapter = new MatchAdapter(matches, userID, getContext());
        this.allMatch.setAdapter(this.adapter);
        this.allMatch.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public List<Match> GetNextMatchs(String date) {
        Log.i(date, "GetNextMatchs date: ");
        final List<Match> matches = new ArrayList<>();
        AndroidNetworking.get(SERVER)
                /*.addQueryParameter("date", "2021-07-22")*/
                .addQueryParameter("date", date)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        /*Log.i(response.toString(), "onResponse: ");*/
                        try {
                            JSONArray jsonArray = response.getJSONArray("docs");
                            int length = jsonArray.length();
                            for (int i = 0; i < length; i++) {
                                JSONObject explrObject = jsonArray.getJSONObject(i);

                                Match match = new Match();

                                match.id = explrObject.getInt("id");

                                Team team1Object = new Team();
                                JSONArray team1Array= explrObject.getJSONArray("team_1");
                                JSONObject teams1 = team1Array.getJSONObject(0);
                                team1Object.id = teams1.getInt("id");
                                team1Object.name = teams1.getString("name");
                                team1Object.logo = teams1.getString("logo");
                                team1Object.id_league = teams1.getInt("id_league");

                                match.team_1 = team1Object;

                                Team team2Object = new Team();
                                JSONArray team2Array= explrObject.getJSONArray("team_2");
                                JSONObject teams2 = team2Array.getJSONObject(0);
                                team2Object.id = teams2.getInt("id");
                                team2Object.name = teams2.getString("name");
                                team2Object.logo = teams2.getString("logo");
                                team2Object.id_league = teams2.getInt("id_league");

                                match.team_2 = team2Object;

                                try {
                                    int score_1 = explrObject.getInt("score_1");
                                    int score_2 = explrObject.getInt("score_2");
                                    if(score_1 != 0 && score_2 != 0){
                                        match.score_1 = score_1;
                                        match.score_2 = score_2;
                                    }
                                    else {
                                        match.score_1 = 0;
                                        match.score_2 = 0;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                match.date_time = explrObject.getString("date_time");
                                match.date = explrObject.getString("date");
                                match.time = explrObject.getString("time");
                                match.quote_team1 = explrObject.getDouble("quote_team1");
                                match.quote_null = explrObject.getDouble("quote_null");
                                match.quote_team2 = explrObject.getDouble("quote_team2");
                                match.popularite = explrObject.getString("popularite");
                                String idwin = explrObject.getString("id_win");
                                if(idwin != "null"){
                                    match.id_win = Integer.valueOf(idwin);
                                }
                                else match.id_win = 0;

                                matches.add(match);

                                MatchSQLite matchSQLite = new MatchSQLite();
                                matchSQLite.team_1_id = match.team_1.id;
                                matchSQLite.team_1_name = match.team_1.name;
                                matchSQLite.team_2_id = match.team_2.id;
                                matchSQLite.team_2_name = match.team_2.name;
                                matchSQLite.score_1 = match.score_1;
                                matchSQLite.score_2 = match.score_2;
                                matchSQLite.date_time = match.date_time;
                                matchSQLite.date = match.date;
                                matchSQLite.time = match.time;
                                matchSQLite.quote_team1 = match.quote_team1;
                                matchSQLite.quote_team2 = match.quote_team2;
                                matchSQLite.quote_null = match.quote_null;
                                matchSQLite.popularite = match.popularite;
                                matchSQLite.id_win = match.id_win;

                                database.matchService().insert(matchSQLite);
                            }
                            adapter.matches = matches;
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        return;
                    }
                });
        return matches;
    }
}