package com.mbds.frontoffice_mobile_pari.Services;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.mbds.frontoffice_mobile_pari.Model.UserSQLite;

import java.util.List;

@Dao
public interface UserService {
    @Query("SELECT * FROM UserSQLite")
    List<UserSQLite> getAll();

    @Query("SELECT * FROM usersqlite ORDER BY id DESC LIMIT 1")
    UserSQLite findLast();

    @Insert
    void insert(UserSQLite userSQLite);

    @Delete
    void delete(UserSQLite user);
}
