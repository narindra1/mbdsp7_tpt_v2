package com.mbds.frontoffice_mobile_pari.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mbds.frontoffice_mobile_pari.Model.PariDetails;
import com.mbds.frontoffice_mobile_pari.Model.PariDetailsSQLite;
import com.mbds.frontoffice_mobile_pari.Model.PariSQLite;
import com.mbds.frontoffice_mobile_pari.R;
import com.mbds.frontoffice_mobile_pari.Services.AppDatabase;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.List;

public class BagCustomAdapter extends RecyclerView.Adapter<BagCustomAdapter.BagViewHolder>{

    public List<PariDetailsSQLite> pariDetails;

    AppDatabase database;
    TextView totalAmount;
    PariSQLite pariSQLite;

    public BagCustomAdapter(List<PariDetailsSQLite> pariDetails, AppDatabase database, PariSQLite pariSQLite, TextView totalAmount) {
        this.pariDetails = pariDetails;
        this.database = database;
        this.totalAmount = totalAmount;
        this.pariSQLite = pariSQLite;
    }

    @NonNull
    @Override
    public BagCustomAdapter.BagViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_details_bet, parent, false);
        return new BagViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BagCustomAdapter.BagViewHolder holder, final int position) {

        holder.date.setText(pariDetails.get(position).date);
        holder.team1.setText(pariDetails.get(position).teamname +" : "+ pariDetails.get(position).teamquote);
        holder.amount.setText("Montant : Ar " + pariDetails.get(position).amount);
        holder.potentialGain.setText("Gain potentiel : Ar "+ pariDetails.get(position).amountwithquote);

        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PariDetailsSQLite pari = pariDetails.get(holder.getAdapterPosition());
                database.pariDetailsService().delete(pari);

                Double oldTotalAmount = pariSQLite.totalAmount;
                Double newTotal = oldTotalAmount - pariDetails.get(position).amount;
                database.pariService().update(newTotal, pariSQLite.id);

                int position = holder.getAdapterPosition();
                pariDetails.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, pariDetails.size());

                totalAmount.setText("Ar " + newTotal);
                pariSQLite.totalAmount = newTotal;
            }
        });

        holder.edit_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.new_amount.setVisibility(View.VISIBLE);
                holder.change_amount.setVisibility(View.VISIBLE);
                holder.new_amount.setText(String.valueOf(pariDetails.get(position).amount));
            }
        });
        holder.change_amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double new_amount = Double.valueOf(holder.new_amount.getText().toString());
                Double potential_gain = new_amount + (new_amount * pariDetails.get(position).teamquote);
                database.pariDetailsService().update(new_amount, potential_gain, pariDetails.get(position).id);
                holder.new_amount.setVisibility(View.INVISIBLE);
                holder.change_amount.setVisibility(View.INVISIBLE);

                holder.amount.setText("Montant : Ar " + new_amount);
                holder.potentialGain.setText("Gain potentiel : Ar "+ potential_gain);

                Double oldTotalAmount = pariSQLite.totalAmount;
                Double newTotal = oldTotalAmount - pariDetails.get(position).amount + new_amount;
                database.pariService().update(newTotal, pariSQLite.id);

                totalAmount.setText("Ar " + newTotal);
                pariSQLite.totalAmount = newTotal;
            }
        });
    }

    @Override
    public int getItemCount() {
        return pariDetails.size();
    }

    public class BagViewHolder extends RecyclerView.ViewHolder {

        private TextView date;
        private TextView team1;
        private EditText new_amount;
        private Button change_amount;
        private TextView amount;
        private TextView potentialGain;
        private ImageView deleteBtn;
        private ImageView edit_Btn;

        public BagViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.date);
            team1 = itemView.findViewById(R.id.team1);
            new_amount = itemView.findViewById(R.id.new_amount);
            change_amount = itemView.findViewById(R.id.change_amount);
            amount = itemView.findViewById(R.id.amount);
            potentialGain = itemView.findViewById(R.id.potentialGain);
            deleteBtn = itemView.findViewById(R.id.deleteBtn);
            edit_Btn = itemView.findViewById(R.id.edit_Btn);
        }
    }
}
