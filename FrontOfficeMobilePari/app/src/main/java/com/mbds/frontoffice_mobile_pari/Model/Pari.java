package com.mbds.frontoffice_mobile_pari.Model;

import java.util.ArrayList;
import java.util.Date;

public class Pari {
    private String Id;
    public String datePari;
    public double totalAmount;
    public boolean isPayed;
    public int idUser;
    public ArrayList<PariDetails> pariDetails;
}
