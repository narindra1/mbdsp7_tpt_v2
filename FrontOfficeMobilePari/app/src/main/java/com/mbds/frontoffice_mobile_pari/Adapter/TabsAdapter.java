package com.mbds.frontoffice_mobile_pari.Adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabsAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragment;
    private List<String> TitleTabs;

    public TabsAdapter(@NonNull FragmentManager fm, int behavior, List<Fragment> fragmentList, List<String> listTitle) {
        super(fm, behavior);
        this.fragment = new ArrayList<>();
        this.TitleTabs = new ArrayList<>();
        this.fragment.addAll(fragmentList);
        this.TitleTabs.addAll(listTitle);
    }

    public TabsAdapter(FragmentManager childFragmentManager) {
        super(childFragmentManager);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return TitleTabs.get(position);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        return this.fragment.get(position);
    }

    @Override
    public int getCount() {
        return fragment.size();
    }
}
