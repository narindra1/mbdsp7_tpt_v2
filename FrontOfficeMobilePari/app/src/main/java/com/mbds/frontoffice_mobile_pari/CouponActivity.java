package com.mbds.frontoffice_mobile_pari;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.JsonObject;
import com.mbds.frontoffice_mobile_pari.Model.PariDetailsSQLite;
import com.mbds.frontoffice_mobile_pari.Model.PariSQLite;
import com.mbds.frontoffice_mobile_pari.Services.AppDatabase;
import com.mbds.frontoffice_mobile_pari.Services.GlobalService;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

public class CouponActivity extends AppCompatActivity {

    TextView match_1;
    TextView macth_2;
    TextView Gain_Txt;
    TextView Mise_Txt;
    EditText amount_win;
    EditText amount_goal;
    TextView team_parie, quote_team, connexion_status_coupon;
    Button validate_bet;
    String teamID = "";
    Double quote;
    String matchID = "";
    String userID = "";
    String type = "pari_gagnant";
    String team_name;

    double potentialgain = 0;
    int totalamount = 0;
    private static final String SERVER = "https://tptnode.herokuapp.com/api/pari";

    AppDatabase database;
    GlobalService globalService = new GlobalService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);

        database = AppDatabase.getInstance(this);

        match_1 = findViewById(R.id.macth_1);
        Gain_Txt = findViewById(R.id.Gain_Txt);
        Mise_Txt = findViewById(R.id.Mise_Txt);
        amount_win = findViewById(R.id.amount_win);
        team_parie = findViewById(R.id.team_parie);
        quote_team = findViewById(R.id.quote_team);
        validate_bet = findViewById(R.id.validate_bet);

        boolean isConnected = globalService.isNetworkConnected(this);
        if(!isConnected){
            connexion_status_coupon = findViewById(R.id.connexion_status_coupon);
            connexion_status_coupon.setVisibility(View.VISIBLE);
        }

        Bundle bundle = getIntent().getExtras();
        final String macth = bundle.getString("MATCH");
        teamID = bundle.getString("TEAMID");
        quote = bundle.getDouble("QUOTE");
        matchID = bundle.getString("MATCHID");
        userID = bundle.getString("USERID");
        team_name = bundle.getString("team_name");

        match_1.setText(macth);
        quote_team.setText(String.valueOf(quote));
        team_parie.setText("Pari sur : " + team_name);

        amount_win.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0){
                    String amountwin= s.toString();
                    int amount_win = Integer.parseInt(amountwin);

                    /*String amountgoal = amount_goal.getText().toString().trim();
                    if ((!TextUtils.isEmpty(amountgoal))){
                        totalamount = amount_win + Integer.parseInt(amountgoal);
                        Mise_Txt.setText(String.valueOf(totalamount));
                        double quote_parsed = Double.parseDouble(quote);
                        potentialgain = totalamount + (totalamount * quote_parsed);
                        Gain_Txt.setText(String.valueOf(potentialgain));
                    }*/
                    /*else {*/
                        Mise_Txt.setText("Ar " + amount_win);
                        /*double quote_parsed = quote;*/
                        potentialgain = amount_win + (amount_win * quote);
                        Gain_Txt.setText("Ar " + potentialgain);
                    /*}*/
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        /*amount_goal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0){
                    String amountgoal = s.toString();
                    int amount_goal = Integer.parseInt(amountgoal);

                    String amountwin = amount_win.getText().toString().trim();
                    if ((!TextUtils.isEmpty(amountwin))){
                        totalamount = amount_goal + Integer.parseInt(amountwin);
                        Mise_Txt.setText(String.valueOf(totalamount));
                        double quote_parsed = Double.parseDouble(quote);
                        potentialgain = totalamount + (totalamount * quote_parsed);
                        Gain_Txt.setText(String.valueOf(potentialgain));
                    }
                    else {
                        Mise_Txt.setText(amountgoal);
                        double quote_parsed = Double.parseDouble(quote);
                        potentialgain = amount_goal + (amount_goal * quote_parsed);
                        Gain_Txt.setText(String.valueOf(potentialgain));
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/

        validate_bet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String amountwithquote1 = Gain_Txt.getText().toString();
                String amount1 = Mise_Txt.getText().toString();

                String amountwithquote = amountwithquote1.split(" ")[1];
                String amount = amount1.split(" ")[1];

                long millis=System.currentTimeMillis();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String date = dateFormat.format(millis);

                PariSQLite parinotpayed = database.pariService().getNotPayed();

                if (parinotpayed != null){
                    PariDetailsSQLite pariDetailsSQLite = new PariDetailsSQLite();
                    pariDetailsSQLite.matchid = Integer.valueOf(matchID);
                    pariDetailsSQLite.teamparie = Integer.valueOf(teamID);
                    pariDetailsSQLite.teamname = team_name;
                    pariDetailsSQLite.teamquote = Double.valueOf(quote);
                    pariDetailsSQLite.amount = Double.valueOf(amount);
                    pariDetailsSQLite.amountwithquote = Double.valueOf(amountwithquote);
                    pariDetailsSQLite.isfinished =  false;
                    pariDetailsSQLite.date = date;
                    pariDetailsSQLite.type = type;
                    pariDetailsSQLite.idpari = parinotpayed.id;

                    database.pariDetailsService().insert(pariDetailsSQLite);

                    Double newAmount = parinotpayed.totalAmount + Double.valueOf(amount);
                    database.pariService().update(newAmount, parinotpayed.id);
                    Toast.makeText(CouponActivity.this, "Pari inséré *****", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CouponActivity.this, BagActivity.class);
                    startActivity(intent);
                }
                else {
                    PariSQLite pariSQLite = new PariSQLite();
                    pariSQLite.datepari = date;
                    pariSQLite.totalAmount = Double.parseDouble(amount);
                    pariSQLite.isPayed = false;
                    pariSQLite.idUser = Integer.valueOf(userID);

                    long pariInsert = database.pariService().insert(pariSQLite);

                    PariDetailsSQLite pariDetailsSQLite = new PariDetailsSQLite();
                    pariDetailsSQLite.matchid = Integer.valueOf(matchID);
                    pariDetailsSQLite.teamparie = Integer.valueOf(teamID);
                    pariDetailsSQLite.teamname = team_name;
                    pariDetailsSQLite.teamquote = Double.valueOf(quote);
                    pariDetailsSQLite.amount = Double.valueOf(amount);
                    pariDetailsSQLite.amountwithquote = Double.valueOf(amountwithquote);
                    pariDetailsSQLite.isfinished =  false;
                    pariDetailsSQLite.date = date;
                    pariDetailsSQLite.type = type;
                    pariDetailsSQLite.idpari = pariInsert;

                    database.pariDetailsService().insert(pariDetailsSQLite);

                    Toast.makeText(CouponActivity.this, "Pari inséré *****", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(CouponActivity.this, BagActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    public void InsertPari(JSONObject jsonObject){

        AndroidNetworking.post(SERVER)
                .addJSONObjectBody(jsonObject)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(CouponActivity.this, "Pari ajouté !!!", Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onError(ANError error) {
                            return;
                    }
                });
    }
}