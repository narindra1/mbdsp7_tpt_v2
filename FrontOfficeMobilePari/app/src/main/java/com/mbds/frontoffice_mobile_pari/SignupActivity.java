package com.mbds.frontoffice_mobile_pari;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupActivity extends AppCompatActivity {

    EditText FullnameTxt;
    EditText UsernameTxt;
    EditText EmailTxt;
    EditText AddressTxt;
    EditText BirthdayTxt;
    EditText PasswordTxt;
    Button Signup_Btn;
    /*private static final String SERVER = "http://192.168.100.100:8010/api/users";*/
    private static final String SERVER = "https://tptnode.herokuapp.com/api/users";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        FullnameTxt = findViewById(R.id.FullnameTxt);
        UsernameTxt = findViewById(R.id.UsernameTxt);
        EmailTxt = findViewById(R.id.EmailTxt);
        AddressTxt = findViewById(R.id.AddressTxt);
        BirthdayTxt = findViewById(R.id.BirthdayTxt);
        PasswordTxt = findViewById(R.id.PasswordTxt);
        Signup_Btn = findViewById(R.id.Signup_Btn);

        Signup_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fullname = FullnameTxt.getText().toString().trim();
                String username = UsernameTxt.getText().toString().trim();
                String email = EmailTxt.getText().toString().trim();
                String adresse = AddressTxt.getText().toString().trim();
                String birthday = BirthdayTxt.getText().toString().trim();
                String password = PasswordTxt.getText().toString().trim();

                SignUp(fullname, username, email, adresse, birthday, password);
            }
        });
    }

    void updateUI(String username)
    {
        if (username != null)
            Toast.makeText(SignupActivity.this, username + " is connected! ", Toast.LENGTH_LONG).show();
    }

    public String SignUp(final String name, final String username, String email, String address, String birthday, String password){
        final String token = "";
        AndroidNetworking.post(SERVER)
                .addBodyParameter("name", name)
                .addBodyParameter("username", username)
                .addBodyParameter("email", email)
                .addBodyParameter("address", address)
                .addBodyParameter("birthday", birthday)
                .addBodyParameter("password", password)
                .addBodyParameter("isAdmin", "false")
                .addBodyParameter("isEnable", "false")
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String token = response.getString("token");
                            UsernameTxt.setText(token);

                            updateUI(name);
                            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                            intent.putExtra("EXTRA_TOKEN", token);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(SignupActivity.this, "Authentication failed", Toast.LENGTH_LONG).show();
                        updateUI(null);
                        return;
                    }
                });

        return token;
    }
}