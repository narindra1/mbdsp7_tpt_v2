package com.mbds.frontoffice_mobile_pari.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class PariDetailsSQLite implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "matchid")
    public int matchid;
    @ColumnInfo(name = "teamparie")
    public int teamparie;
    @ColumnInfo(name = "teamname")
    public String teamname;
    @ColumnInfo(name = "teamquote")
    public Double teamquote;
    @ColumnInfo(name = "amount")
    public Double amount;
    @ColumnInfo(name = "amountwithquote")
    public Double amountwithquote;
    @ColumnInfo(name = "isfinished")
    public boolean isfinished;
    @ColumnInfo(name = "date")
    public String date;
    @ColumnInfo(name = "type")
    public String type;
    @ColumnInfo(name = "idpari")
    public long idpari;
}
