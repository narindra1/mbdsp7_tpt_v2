package com.mbds.frontoffice_mobile_pari.Model;

public interface QRCodeFoundListener {
    void onQRCodeFound(String qrCode);
    void qrCodeNotFound();
}
