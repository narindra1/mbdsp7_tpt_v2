package com.mbds.frontoffice_mobile_pari.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class UserSQLite {

    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "token")
    public String token;
    @ColumnInfo(name = "userid")
    public String userid;
}
