package com.mbds.frontoffice_mobile_pari.Services;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.mbds.frontoffice_mobile_pari.Model.Article;
import com.mbds.frontoffice_mobile_pari.Model.MatchSQLite;
import com.mbds.frontoffice_mobile_pari.Model.PariDetailsSQLite;
import com.mbds.frontoffice_mobile_pari.Model.PariSQLite;
import com.mbds.frontoffice_mobile_pari.Model.UserSQLite;

@Database(entities = {UserSQLite.class, PariSQLite.class, PariDetailsSQLite.class, Article.class, MatchSQLite.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    /*public abstract UserService userService();*/
    private static AppDatabase database;
    private static String database_name = "Bet_database";

    public synchronized static  AppDatabase getInstance(Context context){
        if (database == null){
            database = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, database_name)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return database;
    }

    public abstract UserService userService();
    public abstract ArticleService articleService();
    public abstract PariService pariService();
    public abstract PariDetailsService pariDetailsService();
    public abstract MatchService matchService();
}