package com.mbds.frontoffice_mobile_pari.Model;

public class Match {
    public int id;
    public Team team_1;
    public Team team_2;
    public int score_1;
    public int score_2;
    public String date_time;
    public String date;
    public String time;
    public double quote_team1;
    public double quote_team2;
    public double quote_null;
    public String popularite;
    public int id_win;
}
