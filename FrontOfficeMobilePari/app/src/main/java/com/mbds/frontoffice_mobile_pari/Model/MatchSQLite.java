package com.mbds.frontoffice_mobile_pari.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class MatchSQLite {

    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "team_1_id")
    public int team_1_id;
    @ColumnInfo(name = "team_1_name")
    public String team_1_name;
    @ColumnInfo(name = "team_2_id")
    public int team_2_id;
    @ColumnInfo(name = "team_2_name")
    public String team_2_name;
    @ColumnInfo(name = "score_1")
    public int score_1;
    @ColumnInfo(name = "score_2")
    public int score_2;
    @ColumnInfo(name = "date_time")
    public String date_time;
    @ColumnInfo(name = "date")
    public String date;
    @ColumnInfo(name = "time")
    public String time;
    @ColumnInfo(name = "quote_team1")
    public double quote_team1;
    @ColumnInfo(name = "quote_team2")
    public double quote_team2;
    @ColumnInfo(name = "quote_null")
    public double quote_null;
    @ColumnInfo(name = "popularite")
    public String popularite;
    @ColumnInfo(name = "id_win")
    public int id_win;
}
