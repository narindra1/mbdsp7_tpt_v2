package com.mbds.frontoffice_mobile_pari;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mbds.frontoffice_mobile_pari.Services.GlobalService;

public class ArticleActivity extends AppCompatActivity {

    ImageView pageoneView;
    TextView ArticleTxt, connexion_status_article;
    GlobalService globalService = new GlobalService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);

        pageoneView = findViewById(R.id.pageoneView);
        ArticleTxt = findViewById(R.id.description);
        connexion_status_article = findViewById(R.id.connexion_status_article);

        boolean isConnected = globalService.isNetworkConnected(this);
        if (!isConnected){
            connexion_status_article.setVisibility(View.VISIBLE);
        }

        Bundle bundle = getIntent().getExtras();
        String description = bundle.getString("DESCRIPTION");
        String image = bundle.getString("IMAGE");
        if (!image.isEmpty()){
            LoadingPictures(image, (ImageView) findViewById(R.id.pageoneView));
        }

        ArticleTxt.setText(description);
    }

    private void LoadingPictures(String url, ImageView imageView){
        Glide.with(this)
                .load(url)
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);
    }
}