package com.mbds.frontoffice_mobile_pari;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.common.util.concurrent.ListenableFuture;
import com.mbds.frontoffice_mobile_pari.Model.QRCodeFoundListener;
import com.mbds.frontoffice_mobile_pari.Model.QRCodeImageAnalyzer;
import com.mbds.frontoffice_mobile_pari.Model.UserSQLite;
import com.mbds.frontoffice_mobile_pari.Services.AppDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class QRCodeActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CAMERA = 0;
    PreviewView previewView;
    ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    /*Button qrcode_Btn, button2;*/
    String qrCode;

    private static final String SERVER = "https://tptnode.herokuapp.com/api/user/login";
    private AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        database = AppDatabase.getInstance(this);

        previewView = findViewById(R.id.activity_main_previewView);

        /*qrcode_Btn = (Button) findViewById(R.id.qrcode_Btn);
        qrcode_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), qrCode, Toast.LENGTH_SHORT).show();
                Log.i(QRCodeActivity.class.getSimpleName(), "QR Code Found: " + qrCode);
            }
        });*/

        cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        requestCamera();
    }

    private void requestCamera() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            startCamera();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(QRCodeActivity.this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CAMERA);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CAMERA);
            }
        }
    }


    private void startCamera() {
        cameraProviderFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                bindCameraPreview(cameraProvider);
            } catch (ExecutionException | InterruptedException e) {
                Toast.makeText(this, "Error starting camera " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }, ContextCompat.getMainExecutor(this));
    }

    private void bindCameraPreview(@NonNull ProcessCameraProvider cameraProvider) {
        previewView.setPreferredImplementationMode(PreviewView.ImplementationMode.SURFACE_VIEW);

        Preview preview = new Preview.Builder()
                .build();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();

        preview.setSurfaceProvider(previewView.createSurfaceProvider());

        ImageAnalysis imageAnalysis =
                new ImageAnalysis.Builder()
                        .setTargetResolution(new Size(1280, 720))
                        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                        .build();

        imageAnalysis.setAnalyzer(ContextCompat.getMainExecutor(this), new QRCodeImageAnalyzer(new QRCodeFoundListener() {
            @Override
            public void onQRCodeFound(String _qrCode) {
                qrCode = _qrCode;
                /*qrcode_Btn.setVisibility(View.VISIBLE);*/
                /*Toast.makeText(getApplicationContext(), _qrCode, Toast.LENGTH_SHORT).show();*/
                Log.i(QRCodeActivity.class.getSimpleName(), "QR Code Found: " + _qrCode);
                String[] userInfo = _qrCode.split(" ");
                String username = userInfo[0].trim();
                String password = userInfo[1].trim();

                Log.i(QRCodeActivity.class.getSimpleName(), "userinfo: " + username + " Pqssword : "+ password);

                SingIn(username, password);

                cameraProvider.unbindAll();
            }

            @Override
            public void qrCodeNotFound() {
                /*qrcode_Btn.setVisibility(View.INVISIBLE);*/
            }
        }));

        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner)this, cameraSelector, imageAnalysis, preview);
    }

    public String SingIn(final String username, String password){
        final String token = "";
        AndroidNetworking.post(SERVER)
                .addBodyParameter("username", username)
                .addBodyParameter("password", password)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String token = response.getString("token");
                            String userID = response.getString("iduser");

                            UserSQLite userSQLite = new UserSQLite();
                            userSQLite.token = token;
                            userSQLite.userid = userID;

                            database.userService().insert(userSQLite);

                            Toast.makeText(QRCodeActivity.this, username + " is connected! ", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(QRCodeActivity.this, "Authentication failed", Toast.LENGTH_LONG).show();
                        return;
                    }
                });

        return token;
    }
}