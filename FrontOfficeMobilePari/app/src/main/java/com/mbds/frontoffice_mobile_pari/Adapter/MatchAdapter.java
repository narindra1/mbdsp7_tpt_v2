package com.mbds.frontoffice_mobile_pari.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mbds.frontoffice_mobile_pari.CouponActivity;
import com.mbds.frontoffice_mobile_pari.Model.Match;
import com.mbds.frontoffice_mobile_pari.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.MatchViewHolder>{

    public List<Match> matches;
    public Context context;
    String userID;

    public MatchAdapter(List<Match> matches, String userID, Context context){
        this.matches = matches;
        this.context = context;
        this.userID = userID;
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public MatchAdapter.MatchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_match_details, parent, false);
        return new MatchAdapter.MatchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchAdapter.MatchViewHolder holder, int position) {
        holder.Team1.setText(matches.get(position).team_1.name);
        holder.Team2.setText(matches.get(position).team_2.name);
        holder.score.setText(matches.get(position).score_1 +" - "+ matches.get(position).score_2);
        holder.time.setText(matches.get(position).time);
        holder.Bet_Team1_Btn.setText(String.valueOf(matches.get(position).quote_team1));
        holder.Bet_Team2_Btn.setText(String.valueOf(matches.get(position).quote_team2));
        holder.Bet_Null_Btn.setText(String.valueOf(matches.get(position).quote_null));
        String url_team_1 = "https://tptnode.herokuapp.com/static/image/teams/" + matches.get(position).team_1.logo;
        String url_team_2 = "https://tptnode.herokuapp.com/static/image/teams/" + matches.get(position).team_2.logo;
        LoadingPictures(url_team_1, holder.Image_team1);
        LoadingPictures(url_team_2, holder.Image_team2);

        final int pos = position;

        holder.Bet_Team1_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CouponActivity.class);
                intent.putExtra("MATCH", matches.get(pos).team_1.name +" VS " +matches.get(pos).team_2.name);
                intent.putExtra("team_name", matches.get(pos).team_1.name);
                intent.putExtra("TEAMID", String.valueOf(matches.get(pos).team_1.id));
                intent.putExtra("QUOTE", matches.get(pos).quote_team1);
                intent.putExtra("MATCHID", String.valueOf(matches.get(pos).id));
                intent.putExtra("USERID", userID);
                context.startActivity(intent);
            }
        });
        holder.Bet_Team2_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CouponActivity.class);
                intent.putExtra("MATCH", matches.get(pos).team_1.name +" VS " +matches.get(pos).team_2.name);
                intent.putExtra("team_name", matches.get(pos).team_2.name);
                intent.putExtra("TEAMID", String.valueOf(matches.get(pos).team_2.id));
                intent.putExtra("QUOTE", String.valueOf(matches.get(pos).quote_team2));
                intent.putExtra("MATCHID", String.valueOf(matches.get(pos).id));
                intent.putExtra("USERID", userID);
                context.startActivity(intent);
            }
        });
        holder.Bet_Null_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CouponActivity.class);
                intent.putExtra("MATCH", matches.get(pos).team_1.name +" VS " +matches.get(pos).team_2.name);
                intent.putExtra("team_name", "Pari match null");
                intent.putExtra("TEAMID", String.valueOf(0));
                intent.putExtra("QUOTE", matches.get(pos).quote_null);
                intent.putExtra("MATCHID", String.valueOf(matches.get(pos).id));
                intent.putExtra("USERID", userID);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return matches.size();
    }

    public class MatchViewHolder extends RecyclerView.ViewHolder{
        TextView Team1;
        Button Bet_Team1_Btn;
        Button Bet_Null_Btn;
        TextView Team2;
        Button Bet_Team2_Btn;
        TextView score;
        TextView time;
        ImageView Image_team1;
        ImageView Image_team2;

        public MatchViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            Team1 = itemView.findViewById(R.id.Team1);
            Team2 = itemView.findViewById(R.id.Team2);
            Bet_Team1_Btn = itemView.findViewById(R.id.Bet_Team1_Btn);
            Bet_Null_Btn = itemView.findViewById(R.id.Bet_Null_Btn);
            Bet_Team2_Btn = itemView.findViewById(R.id.Bet_Team2_Btn);
            score = itemView.findViewById(R.id.score);
            time = itemView.findViewById(R.id.time);
            Image_team1 = itemView.findViewById(R.id.Image_team1);
            Image_team2 = itemView.findViewById(R.id.Image_team2);

        }
    }
    private void LoadingPictures(String url, ImageView imageView){
        Glide.with(this.context)
                .load(url)
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);
    }
}
