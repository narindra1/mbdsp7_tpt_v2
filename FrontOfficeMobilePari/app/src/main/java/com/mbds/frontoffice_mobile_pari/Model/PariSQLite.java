package com.mbds.frontoffice_mobile_pari.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class PariSQLite implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "datepari")
    public String datepari;
    @ColumnInfo(name = "totalAmount")
    public Double totalAmount;
    @ColumnInfo(name = "isPayed")
    public Boolean isPayed;
    @ColumnInfo(name = "idUser")
    public int idUser;
}
