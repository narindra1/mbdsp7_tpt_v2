package com.mbds.frontoffice_mobile_pari;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mbds.frontoffice_mobile_pari.Adapter.BagCustomAdapter;
import com.mbds.frontoffice_mobile_pari.Model.Pari;
import com.mbds.frontoffice_mobile_pari.Model.PariDetails;
import com.mbds.frontoffice_mobile_pari.Model.PariDetailsSQLite;
import com.mbds.frontoffice_mobile_pari.Model.PariSQLite;
import com.mbds.frontoffice_mobile_pari.Model.User;
import com.mbds.frontoffice_mobile_pari.Model.UserSQLite;
import com.mbds.frontoffice_mobile_pari.Services.AppDatabase;
import com.mbds.frontoffice_mobile_pari.Services.GlobalService;
import com.mbds.frontoffice_mobile_pari.Services.PariDetailsService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BagActivity extends AppCompatActivity {

    private static final String SERVER = "https://tptnode.herokuapp.com/api/pari/custom";

    FloatingActionButton Payment_Btn;
    RecyclerView allBet;
    TextView amouttotal, connexion_status_bag;

    private List<PariDetailsSQLite> pariDetailsServices = new ArrayList<>();
    private BagCustomAdapter adapter;

    User user = new User(this.getApplication());

    String pariID = "";
    String matchID = "";
    String reference = "REF";

    AppDatabase database;
    PariSQLite pariSQLite;
    GlobalService globalService = new GlobalService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bag);

        Payment_Btn = findViewById(R.id.Payment_Btn);
        allBet = findViewById(R.id.allBet);
        amouttotal = findViewById(R.id.amouttotal);
        connexion_status_bag = findViewById(R.id.connexion_status_bag);

        database = AppDatabase.getInstance(this);

        boolean isConnected = globalService.isNetworkConnected(this);
        if (!isConnected)
            connexion_status_bag.setVisibility(View.VISIBLE);

        UserSQLite userSQLite = database.userService().findLast();
        final String userID = userSQLite.userid;
        /*pari = GetPariInProgress(userID);*/

        pariSQLite = database.pariService().getNotPayed();
        if (pariSQLite != null){
            pariDetailsServices = database.pariDetailsService().getByPari(pariSQLite.id);

            Log.i(pariSQLite.toString(), "pariSQLite: ");

            amouttotal.setText("Ar "+ pariSQLite.totalAmount);
        }else {
            amouttotal.setText("Ar 0");
        }

        setRecyclerView();

        Payment_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BagActivity.this, PaymentActivity.class);
                intent.putExtra("pariID", pariSQLite.id);
                intent.putExtra("userID", userID);
                reference += "" + userID + "" + pariSQLite.id;
                intent.putExtra("reference", reference);
                intent.putExtra("amount", String.valueOf(pariSQLite.totalAmount));
                Bundle bundle = new Bundle();
                bundle.putSerializable("pariSQLite", pariSQLite);
                bundle.putSerializable("pariDetailsServices", (Serializable) pariDetailsServices);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        adapter.pariDetails = pariDetailsServices;
        adapter.notifyDataSetChanged();
    }

    private void setRecyclerView(){
        this.adapter = new BagCustomAdapter(pariDetailsServices, database, pariSQLite, amouttotal);
        this.allBet.setAdapter(this.adapter);
        this.allBet.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    public Pari GetPariInProgress(String userId){
        final Pari paris = new Pari();
        AndroidNetworking.get(SERVER)
                .addQueryParameter("iduser", userId)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.i(response.toString(), "onResponse: ");
                        try {
                            int length = response.length();
                            for (int i = 0; i<length; i++){
                                JSONObject explrObject = response.getJSONObject(i);
                                pariID = explrObject.getString("id");
                                paris.datePari = explrObject.getString("datePari");
                                Double totalAmount = explrObject.getDouble("totalAmount");
                                paris.totalAmount = totalAmount;
                                amouttotal.setText(String.valueOf(totalAmount));
                                paris.isPayed = explrObject.getBoolean("isPayed");
                                paris.idUser = explrObject.getInt("idUser");
                                JSONArray jsonArray = explrObject.getJSONArray("pariMise");
                                int detailSize = jsonArray.length();
                                List<PariDetails> pariDetails = new ArrayList<>();
                                for (int d = 0; d < detailSize; d++) {
                                    PariDetails pariDetail = new PariDetails();
                                    JSONObject detailsObject = jsonArray.getJSONObject(d);

                                    matchID = detailsObject.getString("idMatch");
                                    pariDetail.idMatch = Integer.parseInt(matchID);
                                    pariDetail.amount = Double.parseDouble(detailsObject.getString("amount"));
                                    pariDetail.idTeamParie = Integer.parseInt(detailsObject.getString("idTeamParie"));
                                    pariDetail.amountWithQuote = detailsObject.getString("amountWithQuote");
                                    pariDetail.isFinished = Boolean.parseBoolean(detailsObject.getString("isFinished"));
                                    pariDetail.dateInsert = detailsObject.getString("dateInsert");
                                    pariDetail.type = detailsObject.getString("type");

                                    pariDetails.add(pariDetail);
                                }
                                paris.pariDetails = (ArrayList<PariDetails>) pariDetails;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        return;
                    }
                });
        return paris;
    }
}