package com.mbds.frontoffice_mobile_pari;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mbds.frontoffice_mobile_pari.Services.GlobalService;

import java.io.Serializable;
import java.text.SimpleDateFormat;

public class SummarizeActivity extends AppCompatActivity {


    TextView transaction_date, threeds, card_type, card_number, transaction_reference, transaction_amount, connexion_status_summarize;
    Button home_Btn;

    GlobalService globalService = new GlobalService();
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summarize);

        transaction_date = findViewById(R.id.transaction_date);
        threeds = findViewById(R.id.threeds);
        card_type = findViewById(R.id.card_type);
        card_number = findViewById(R.id.card_number);
        transaction_reference = findViewById(R.id.transaction_reference);
        transaction_amount = findViewById(R.id.transaction_amount);
        home_Btn = findViewById(R.id.home_Btn);
        connexion_status_summarize = findViewById(R.id.connexion_status_summarize);

        boolean isConnected = globalService.isNetworkConnected(this);
        if (!isConnected)
            connexion_status_summarize.setVisibility(View.VISIBLE);

        long millis=System.currentTimeMillis();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(millis);

        Bundle bundle = getIntent().getExtras();
        final String bankcard = bundle.getString("bankcard");
        final String reference = bundle.getString("reference");
        final String amount = bundle.getString("amount");

        transaction_date.setText(date);
        threeds.setText("OUI");
        card_type.setText("Carte bancaire");
        card_number.setText(bankcard);
        transaction_reference.setText(reference);
        transaction_amount.setText("Ar " + amount);

        home_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SummarizeActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}