package com.mbds.frontoffice_mobile_pari.Model;

public class PariDetails {
    public int idMatch;
    public double amount;
    public int idTeamParie;
    public String amountWithQuote;
    public boolean isFinished;
    public String dateInsert;
    public String type;
    public Pari pari;
}
