package com.mbds.frontoffice_mobile_pari.Services;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.mbds.frontoffice_mobile_pari.Model.PariSQLite;

import java.util.List;

@Dao
public interface PariService {
    @Query("SELECT * FROM PariSQLite")
    List<PariSQLite> getAll();

    @Query("SELECT * FROM PariSQLite WHERE isPayed =0")
    PariSQLite getNotPayed();

    @Query("Update PariSQLite SET totalAmount = :amount WHERE id = :id")
    void update(Double amount, int id);

    @Query("Update PariSQLite SET isPayed =1 WHERE id = :id")
    void updatePariPayed(int id);

    @Insert
    long insert(PariSQLite pariSQLite);

    @Delete
    void delete(PariSQLite pariSQLite);
}
