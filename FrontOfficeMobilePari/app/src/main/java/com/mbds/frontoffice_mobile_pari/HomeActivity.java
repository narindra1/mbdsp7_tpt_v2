package com.mbds.frontoffice_mobile_pari;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.mbds.frontoffice_mobile_pari.Adapter.TabsAdapter;
import com.mbds.frontoffice_mobile_pari.Fragment.BetFragment;
import com.mbds.frontoffice_mobile_pari.Fragment.NewsFragment;
import com.mbds.frontoffice_mobile_pari.Fragment.ResultFragment;
import com.mbds.frontoffice_mobile_pari.Services.GlobalService;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {
    private ArrayList<Fragment> fragment = new ArrayList<Fragment>();;
    private ArrayList<String> TitleTabs = new ArrayList<String>();;
    private NewsFragment newsFragment = new NewsFragment();
    private BetFragment betFragment = new BetFragment();
    private ResultFragment resultFragment = new ResultFragment();

    FloatingActionButton bag_Btn;
    TextView connexion_status;

    GlobalService globalService = new GlobalService();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Init();

        boolean isConnected = globalService.isNetworkConnected(this);
        if(!isConnected){
            connexion_status = findViewById(R.id.connexion_status);
            connexion_status.setVisibility(View.VISIBLE);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Accueil"));
        tabLayout.addTab(tabLayout.newTab().setText("Match"));
        tabLayout.addTab(tabLayout.newTab().setText("Mes paris"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager =(ViewPager)findViewById(R.id.pager);
        TabsAdapter tabsAdapter = new TabsAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), fragment, TitleTabs);
        viewPager.setAdapter(tabsAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }


            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        bag_Btn =  findViewById(R.id.bag_Btn);
        bag_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, BagActivity.class);
                /*Intent intent = new Intent(HomeActivity.this, QRCodeActivity.class);*/
                startActivity(intent);
            }
        });
    }

    private void Init(){
        fragment.add(this.newsFragment);
        fragment.add(this.betFragment);
        fragment.add(this.resultFragment);

        TitleTabs.add("Accueil");
        TitleTabs.add("Match");
        TitleTabs.add("Mes paris");
    }
}