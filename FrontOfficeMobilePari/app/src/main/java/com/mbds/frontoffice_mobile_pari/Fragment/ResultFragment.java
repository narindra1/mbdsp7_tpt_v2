package com.mbds.frontoffice_mobile_pari.Fragment;

import android.database.Cursor;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mbds.frontoffice_mobile_pari.Adapter.HistoricAdapter;
import com.mbds.frontoffice_mobile_pari.Model.Match;
import com.mbds.frontoffice_mobile_pari.Model.Pari;
import com.mbds.frontoffice_mobile_pari.Model.PariDetails;
import com.mbds.frontoffice_mobile_pari.Model.ResultCustom;
import com.mbds.frontoffice_mobile_pari.Model.Team;
import com.mbds.frontoffice_mobile_pari.Model.User;
import com.mbds.frontoffice_mobile_pari.Model.UserSQLite;
import com.mbds.frontoffice_mobile_pari.R;
import com.mbds.frontoffice_mobile_pari.Services.AppDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ResultFragment extends Fragment {

    RecyclerView allHistorics;
    HistoricAdapter adapter;

    User user = new User(getContext());
    List<ResultCustom> historics;

    String userID;

    private static final String SERVER = "https://tptnode.herokuapp.com/api/historic";
    AppDatabase database;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = AppDatabase.getInstance(getContext());

        UserSQLite userSQLite = database.userService().findLast();
        userID = userSQLite.userid;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("iduser", 3);
            jsonObject.put("max", 5);
            jsonObject.put("offset", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        historics = GetHistoric(jsonObject);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_result, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        allHistorics = getView().findViewById(R.id.allHistoric);
        setRecyclerView();
    }

    private void setRecyclerView(){
        this.historics = new ArrayList<>();
        this.adapter = new HistoricAdapter(historics);
        this.allHistorics.setAdapter(this.adapter);
        this.allHistorics.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public List<ResultCustom> GetHistoric(JSONObject jsonObject){
        final List<ResultCustom> resultCustoms = new ArrayList<>();
        AndroidNetworking.post(SERVER)
                .addJSONObjectBody(jsonObject)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener(){

                    @Override
                    public void onResponse(JSONArray response) {

                        Log.i(response.toString(), "onResponse: ");
                        try {
                            int length = response.length();
                            for (int i = 0; i< length; i++){
                                JSONObject explrObject = response.getJSONObject(i);

                                Match match = new Match();
                                JSONArray matchArray = explrObject.getJSONArray("match");
                                JSONObject matchObject = matchArray.getJSONObject(0);
                                matchObject.getString("_id");
                                match.id = matchObject.getInt("id");

                                Team team1Object = new Team();
                                JSONArray team1Array= matchObject.getJSONArray("team_1");
                                JSONObject teams1 = team1Array.getJSONObject(0);
                                teams1.getString("_id");
                                team1Object.id = teams1.getInt("id");
                                team1Object.name = teams1.getString("name");
                                team1Object.logo = teams1.getString("logo");
                                team1Object.id_league = teams1.getInt("id_league");

                                match.team_1 = team1Object;

                                Team team2Object = new Team();
                                JSONArray team2Array= matchObject.getJSONArray("team_2");
                                JSONObject teams2 = team2Array.getJSONObject(0);
                                teams2.getString("_id");
                                team2Object.id = teams2.getInt("id");
                                team2Object.name = teams2.getString("name");
                                team2Object.logo = teams2.getString("logo");
                                team2Object.id_league = teams2.getInt("id_league");

                                match.team_2 = team2Object;

                                try {
                                    int score_1 = explrObject.getInt("score_1");
                                    int score_2 = explrObject.getInt("score_2");
                                    if(score_1 != 0 && score_2 != 0){
                                        match.score_1 = score_1;
                                        match.score_2 = score_2;
                                    }
                                    else {
                                        match.score_1 = 0;
                                        match.score_2 = 0;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                match.date_time = matchObject.getString("date_time");
                                match.date = matchObject.getString("date");
                                match.time = matchObject.getString("time");
                                match.quote_team1 = matchObject.getDouble("quote_team1");
                                match.quote_null = matchObject.getDouble("quote_null");
                                match.quote_team2 = matchObject.getDouble("quote_team2");
                                match.popularite = matchObject.getString("popularite");
                                String idwin = matchObject.getString("id_win");
                                if(idwin != "null"){
                                    match.id_win = Integer.valueOf(idwin);
                                }
                                else match.id_win = 0;

                                Pari pari = new Pari();
                                JSONObject pariObject = explrObject.getJSONObject("pari");
                                JSONObject parisObject = pariObject.getJSONObject("paris");
                                pari.datePari = parisObject.getString("datePari");
                                pari.totalAmount = parisObject.getDouble("totalAmount");
                                pari.isPayed = parisObject.getBoolean("isPayed");
                                pari.idUser = parisObject.getInt("idUser");

                                PariDetails pariDetails = new PariDetails();
                                pariDetails.idMatch = pariObject.getInt("idMatch");
                                pariDetails.amount = pariObject.getDouble("amount");
                                pariDetails.idTeamParie = pariObject.getInt("idTeamParie");
                                pariDetails.amountWithQuote = pariObject.getString("amountWithQuote");
                                pariDetails.isFinished = pariObject.getBoolean("isFinished");
                                pariDetails.dateInsert = pariObject.getString("dateInsert");
                                pariDetails.type = pariObject.getString("type");
                                pariDetails.pari = pari;

                                double gain = explrObject.getDouble("gain");
                                double perte = explrObject.getDouble("perte");

                                ResultCustom resultCustom = new ResultCustom();
                                resultCustom.match = match;
                                resultCustom.pariDetails = pariDetails;
                                resultCustom.gain = gain;
                                resultCustom.perte = perte;

                                resultCustoms.add(resultCustom);
                            }
                            adapter.historics = resultCustoms;
                            adapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        return;
                    }
                });
        return resultCustoms;
    }
}