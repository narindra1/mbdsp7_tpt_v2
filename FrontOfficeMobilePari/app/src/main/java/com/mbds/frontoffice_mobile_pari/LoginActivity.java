package com.mbds.frontoffice_mobile_pari;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mbds.frontoffice_mobile_pari.Model.UserSQLite;
import com.mbds.frontoffice_mobile_pari.Services.AppDatabase;
import com.mbds.frontoffice_mobile_pari.Services.GlobalService;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    /*User user = new User(this);*/
    EditText UsernameTxt;
    EditText PasswordTxt;
    Button connexion_Btn;
    Button SignUp;
    ImageButton scanner_Btn;
    private static final String SERVER = "https://tptnode.herokuapp.com/api/user/login";

    private AppDatabase database;
    GlobalService globalService = new GlobalService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        AndroidNetworking.initialize(getApplicationContext());

        boolean isConnected = globalService.isNetworkConnected(this);
        if(!isConnected){
            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
            startActivity(intent);
        }

        database = AppDatabase.getInstance(this);

        UsernameTxt = findViewById(R.id.UsernameTxt);
        PasswordTxt = findViewById(R.id.PasswordTxt);
        SignUp = findViewById(R.id.SignUp);
        connexion_Btn = findViewById(R.id.connexion_Btn);
        scanner_Btn = findViewById(R.id.scanner_Btn);

        connexion_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = UsernameTxt.getText().toString().trim();
                String password = PasswordTxt.getText().toString().trim();
                if ((TextUtils.isEmpty(username))){
                    UsernameTxt.setError("Username is Required.");
                    return;
                }
                if ((TextUtils.isEmpty(password))){
                    PasswordTxt.setError("Password is Required.");
                    return;
                }
                SingIn(username, password);
            }
        });

        SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(), SignupActivity.class);
                startActivity(intent);
            }
        });

        scanner_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, QRCodeActivity.class);
                startActivity(intent);
            }
        });
    }

    void updateUI(String username)
    {
        if (username != null)
            Toast.makeText(LoginActivity.this, username + "is connected! ", Toast.LENGTH_LONG).show();
    }

    public String SingIn(final String username, String password){
        final String token = "";
        AndroidNetworking.post(SERVER)
                .addBodyParameter("username", username)
                .addBodyParameter("password", password)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String token = response.getString("token");
                            String userID = response.getString("iduser");

                            /*user.saveUser(token, userID, LoginActivity.this);*/

                            /*AppDatabase db = Room.databaseBuilder(LoginActivity.this,
                                    AppDatabase.class, "Bet_database").build();
                            UserService userService = db.userService();*/

                            UserSQLite userSQLite = new UserSQLite();
                            /*userSQLite.id = 1;*/
                            userSQLite.token = token;
                            userSQLite.userid = userID;
                            /*userService.insert(userSQLite);*/

                            database.userService().insert(userSQLite);

                            Toast.makeText(LoginActivity.this, username + " is connected! ", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
//                            intent.putExtra("EXTRA_USERID", userID);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(LoginActivity.this, "Authentication failed", Toast.LENGTH_LONG).show();
                        updateUI(null);
                        return;
                    }
                });

        return token;
    }
}