package com.mbds.frontoffice_mobile_pari.Services;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import com.mbds.frontoffice_mobile_pari.Model.PariDetailsSQLite;
import java.util.List;

@Dao
public interface PariDetailsService {
    @Query("SELECT * FROM PariDetailsSQLite")
    List<PariDetailsSQLite> getAll();

    @Query("SELECT * FROM PariDetailsSQLite WHERE idpari = :idpari")
    List<PariDetailsSQLite> getByPari(long idpari);

    @Insert
    void insert(PariDetailsSQLite pariDetailsSQLite);

    @Delete
    void delete(PariDetailsSQLite pariDetailsSQLite);

    @Query("UPDATE PariDetailsSQLite SET amount = :new_amount, amountwithquote = :amountwithquote WHERE id = :id")
    void update(Double new_amount, Double amountwithquote, int id);
}
