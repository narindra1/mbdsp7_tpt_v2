package com.mbds.frontoffice_mobile_pari.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class User extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "Bet_database";
    public static final String USER_TABLE_NAME = "user";
    public static final String User_COLUMN_ID = "_id";
    public static final String User_COLUMN_TOKEN = "token";
    public static final String User_COLUMN_USERID = "userid";

    public User(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + USER_TABLE_NAME + " (" +
                User_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                User_COLUMN_TOKEN + " TEXT, " +
                User_COLUMN_USERID + " INT " + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        onCreate(db);
    }

    public void saveUser(String token, String userId, Context context) {
        SQLiteDatabase database = new User(context).getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(User.User_COLUMN_TOKEN, token);
        values.put(User.User_COLUMN_USERID, userId);
        long newRowId = database.insert(User.USER_TABLE_NAME, null, values);

        Toast.makeText(context, "The new Row Id is " + newRowId, Toast.LENGTH_LONG).show();
    }

    public Cursor GetUser(Context context){
        SQLiteDatabase database = new User(context).getReadableDatabase();

        Cursor cursor = database.rawQuery("SELECT * FROM "+ User.USER_TABLE_NAME +" ORDER BY _id DESC LIMIT 1", null);

        return cursor;
    }
}
