    package com.mbds.frontoffice_mobile_pari.Fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.mbds.frontoffice_mobile_pari.ArticleActivity;
import com.mbds.frontoffice_mobile_pari.Model.Article;
import com.mbds.frontoffice_mobile_pari.R;
import com.mbds.frontoffice_mobile_pari.Services.AppDatabase;
import com.mbds.frontoffice_mobile_pari.Services.GlobalService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class NewsFragment extends Fragment {

    ImageView alauneView;
    TextView titleTxt, dateArticle;
            private static final String SERVER = "https://tptnode.herokuapp.com/api/articles";
//    private static final String SERVER = "http://192.168.8.108:8010/api/articles";
    long millis=System.currentTimeMillis();
    Date currentdate = new Date(millis);
    String description = "";
    String picture = "";

    AppDatabase database;
    GlobalService globalService = new GlobalService();
    boolean isConnected = false;
    Article article = new Article();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = AppDatabase.getInstance(getContext());

        isConnected = globalService.isNetworkConnected(getContext());

        if (isConnected){
            GetArticle(currentdate.toString(), 1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        alauneView = (ImageView) getView().findViewById(R.id.alauneView);
        titleTxt = getView().findViewById(R.id.titleTxt);
        dateArticle = getView().findViewById(R.id.dateArticle);

        if(!isConnected){
            article = database.articleService().getLast();
            titleTxt.setText(article.title);
            description = article.description;
        }

        alauneView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ArticleActivity.class);
                if(isConnected){
                    intent.putExtra("DESCRIPTION", description);
                    intent.putExtra("IMAGE", picture);
                }else {
                    intent.putExtra("DESCRIPTION", article.description);
                    intent.putExtra("IMAGE", "picture");
                }

                startActivity(intent);
            }
        });
        titleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ArticleActivity.class);
                intent.putExtra("DESCRIPTION", description);
                intent.putExtra("IMAGE", picture);
                startActivity(intent);
            }
        });
    }

    private void LoadingPictures(String url, ImageView imageView){
        Glide.with(this)
                .load(url)
                .error(R.mipmap.ic_launcher)
                .placeholder(R.mipmap.ic_launcher)
                .into(imageView);
    }

    public void GetArticle(final String date, Integer page){
        AndroidNetworking.get(SERVER)
               /* .addQueryParameter("date", "2021-07-22")*/
                .addQueryParameter("date", date)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("docs");
                            int length = jsonArray.length();
                            for (int i = 0; i < length; i++) {
                                JSONObject explrObject = jsonArray.getJSONObject(i);

                                String title = explrObject.getString("titre");
                                picture = explrObject.getString("image");
                                description = explrObject.getString("description");
                                dateArticle.setText(date);
                                titleTxt.setText(title);
                                String url = "https://tptnode.herokuapp.com/static/image/articles/" + picture;
                                LoadingPictures(url, (ImageView) getView().findViewById(R.id.alauneView));
                                picture = url;
                                Article article = new Article();
                                article.title = title;
                                article.description = description;

                                database.articleService().insert(article);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        return;
                    }
                });
    }
}