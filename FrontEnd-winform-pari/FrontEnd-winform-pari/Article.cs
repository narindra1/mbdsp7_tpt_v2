﻿using FrontEnd_winform_pari.Model;
using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Article : Form
    {
        static HttpClient client = new HttpClient();
        string cellclicked = "";
        public Article()
        {
            InitializeComponent();
            InitGrid();
        }
        public async void InitGrid()
        {
            logo.SizeMode = PictureBoxSizeMode.StretchImage;
            WebClient wc = new WebClient();
            var articles = await new ArticleService().GetArticlesAsync(client);
            foreach (Articles article in articles.docs)
            {
                articleGrid.RowTemplate.Height = 80;
                try { 
                    if (wc.DownloadData("https://tptnode.herokuapp.com/static/image/articles/"+ article.image) != null)
                    {
                        byte[] bytes = wc.DownloadData("https://tptnode.herokuapp.com/static/image/articles/"+ article.image);
                        MemoryStream ms = new MemoryStream(bytes);
                        Image img = Image.FromStream(ms);
                        image.ImageLayout = DataGridViewImageCellLayout.Stretch;
                        articleGrid.Rows.Add(article.id, article.titre, img, article.date.ToString("yyyy-MM-dd"), article.description);
                    }
                }
                catch (Exception wex)
                {
                    //if (((HttpWebResponse)wex.Response).StatusCode == HttpStatusCode.NotFound)
                    //{
                    //    byte[] bytes = wc.DownloadData("https://tptnode.herokuapp.com/static/image/sports/sport-1626771289161.jpg");
                    //    MemoryStream ms = new MemoryStream(bytes);
                    //    Image img = Image.FromStream(ms);
                    //    image.ImageLayout = DataGridViewImageCellLayout.Stretch;
                    //    // articleGrid.RowTemplate.Height = 80;
                    //    articleGrid.Rows.Add(article.id, article.titre, img, article.date.ToString("yyyy-MM-dd"), article.description);
                    //}
                    MessageBox.Show(wex.Message);
                }
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            new Article_action().Show();
        }

        public void ResetGrid()
        {
            articleGrid.Rows.Clear();
            InitGrid();
        }
        private void articleGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.articleGrid.Rows[e.RowIndex];
                cellclicked = row.Cells["ID"].Value.ToString();
                string nameclicked = row.Cells["titre"].Value.ToString();

                MessageBox.Show("Vous avez sélectionné " + nameclicked);
            }
        }
        private async void Delete_clickAsync(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Etes-vous sur de supprimer ?",
                          "Suppression confirmée",
                          MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                await new ArticleService().DeleteArticleAsync(cellclicked, client);
                MessageBox.Show("Le sport a été supprimer avec success !!! ", "Suppression", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ResetGrid();
            }
        }

        private void Label3_Click(object sender, EventArgs e)
        {
            new Dashboard().Show();
        }

        private void Label8_Click(object sender, EventArgs e)
        {
            new Equipes().Show();
        }

        private void Label10_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Match().Show();
        }

        private void Label9_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Sport().Show();
        }
    }
}
