﻿using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Finalizmatch : Form
    {
        static HttpClient client = new HttpClient();
        private int idMatch = 0;
        private int idteamwinner = 0;
        private Form parent;

        public Finalizmatch()
        {
            InitializeComponent();
        }
        public Finalizmatch( int id,string team_1,string team_2 ,Form parents)
        {
            InitializeComponent();
            parent = parents;
            idMatch =id;
            team1.Text = "equipe domicile: "+team_1;
            team2.Text = "equipe exterieur: " + team_2;
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox1.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                textBox1.Text = textBox1.Text.Remove(textBox1.Text.Length - 1);
            }
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(textBox2.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                textBox2.Text = textBox2.Text.Remove(textBox2.Text.Length - 1);
            }
        }

        private async void Add_Click(object sender, EventArgs e)
        {
            if (Int32.Parse(textBox1.Text) < Int32.Parse(textBox2.Text))
            {
                idteamwinner = Int32.Parse(textBox2.Text);
            }
            else if (Int32.Parse(textBox1.Text) > Int32.Parse(textBox2.Text))
            {
                idteamwinner = Int32.Parse(textBox1.Text);
            }
            else {
                idteamwinner = -1;
            }
            await new MatchService().FinalizMatch(client,idMatch, Int32.Parse(textBox1.Text), Int32.Parse(textBox2.Text), idteamwinner);
            parent.Hide();
            this.Hide();
            new Match().Show();
        }
    }
}
