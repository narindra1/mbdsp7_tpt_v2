﻿namespace FrontEnd_winform_pari
{
    partial class Sports_Actions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tresorerie = new System.Windows.Forms.Label();
            this.article = new System.Windows.Forms.Label();
            this.equipe = new System.Windows.Forms.Label();
            this.Sport = new System.Windows.Forms.Label();
            this.match = new System.Windows.Forms.Label();
            this.Photo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adresse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Heure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Actions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.filename = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.add = new System.Windows.Forms.Button();
            this.upload = new System.Windows.Forms.Button();
            this.picture = new System.Windows.Forms.PictureBox();
            this.logo = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel1.Controls.Add(this.logo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 87);
            this.panel1.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(826, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Admnistrateur";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel2.Controls.Add(this.tresorerie);
            this.panel2.Controls.Add(this.article);
            this.panel2.Controls.Add(this.equipe);
            this.panel2.Controls.Add(this.Sport);
            this.panel2.Controls.Add(this.match);
            this.panel2.Location = new System.Drawing.Point(1, 85);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(105, 500);
            this.panel2.TabIndex = 6;
            // 
            // tresorerie
            // 
            this.tresorerie.AutoSize = true;
            this.tresorerie.ForeColor = System.Drawing.Color.White;
            this.tresorerie.Location = new System.Drawing.Point(20, 280);
            this.tresorerie.Name = "tresorerie";
            this.tresorerie.Size = new System.Drawing.Size(54, 13);
            this.tresorerie.TabIndex = 0;
            this.tresorerie.Text = "Trésorerie";
            // 
            // article
            // 
            this.article.AutoSize = true;
            this.article.ForeColor = System.Drawing.Color.White;
            this.article.Location = new System.Drawing.Point(20, 221);
            this.article.Name = "article";
            this.article.Size = new System.Drawing.Size(36, 13);
            this.article.TabIndex = 0;
            this.article.Text = "Article";
            // 
            // equipe
            // 
            this.equipe.AutoSize = true;
            this.equipe.ForeColor = System.Drawing.Color.White;
            this.equipe.Location = new System.Drawing.Point(20, 166);
            this.equipe.Name = "equipe";
            this.equipe.Size = new System.Drawing.Size(40, 13);
            this.equipe.TabIndex = 0;
            this.equipe.Text = "Equipe";
            // 
            // Sport
            // 
            this.Sport.AutoSize = true;
            this.Sport.ForeColor = System.Drawing.SystemColors.Window;
            this.Sport.Location = new System.Drawing.Point(20, 109);
            this.Sport.Name = "Sport";
            this.Sport.Size = new System.Drawing.Size(32, 13);
            this.Sport.TabIndex = 0;
            this.Sport.Text = "Sport";
            // 
            // match
            // 
            this.match.AutoSize = true;
            this.match.ForeColor = System.Drawing.Color.White;
            this.match.Location = new System.Drawing.Point(20, 50);
            this.match.Name = "match";
            this.match.Size = new System.Drawing.Size(37, 13);
            this.match.TabIndex = 0;
            this.match.Text = "Match";
            // 
            // Photo
            // 
            this.Photo.HeaderText = "Equipe 1";
            this.Photo.MinimumWidth = 6;
            this.Photo.Name = "Photo";
            this.Photo.Width = 125;
            // 
            // Nom
            // 
            this.Nom.HeaderText = "Equipe 2";
            this.Nom.MinimumWidth = 6;
            this.Nom.Name = "Nom";
            this.Nom.Width = 125;
            // 
            // Age
            // 
            this.Age.HeaderText = "Score équipe 1";
            this.Age.MinimumWidth = 6;
            this.Age.Name = "Age";
            this.Age.Width = 125;
            // 
            // Email
            // 
            this.Email.HeaderText = "Score équipe 2";
            this.Email.MinimumWidth = 6;
            this.Email.Name = "Email";
            this.Email.Width = 125;
            // 
            // Adresse
            // 
            this.Adresse.HeaderText = "Date";
            this.Adresse.MinimumWidth = 6;
            this.Adresse.Name = "Adresse";
            this.Adresse.Width = 125;
            // 
            // Heure
            // 
            this.Heure.HeaderText = "Heure";
            this.Heure.MinimumWidth = 6;
            this.Heure.Name = "Heure";
            this.Heure.Width = 125;
            // 
            // Actions
            // 
            this.Actions.HeaderText = "Actions";
            this.Actions.MinimumWidth = 6;
            this.Actions.Name = "Actions";
            this.Actions.Width = 125;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(278, 248);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nom :";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(412, 245);
            this.name.Multiline = true;
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(217, 28);
            this.name.TabIndex = 8;
            // 
            // filename
            // 
            this.filename.Location = new System.Drawing.Point(412, 340);
            this.filename.Multiline = true;
            this.filename.Name = "filename";
            this.filename.Size = new System.Drawing.Size(217, 28);
            this.filename.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(278, 343);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Image :";
            // 
            // add
            // 
            this.add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.add.ForeColor = System.Drawing.Color.White;
            this.add.Location = new System.Drawing.Point(468, 429);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(120, 38);
            this.add.TabIndex = 11;
            this.add.Text = "Enregistrer";
            this.add.UseVisualStyleBackColor = false;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // upload
            // 
            this.upload.BackColor = System.Drawing.Color.Black;
            this.upload.ForeColor = System.Drawing.Color.White;
            this.upload.Location = new System.Drawing.Point(670, 340);
            this.upload.Name = "upload";
            this.upload.Size = new System.Drawing.Size(102, 28);
            this.upload.TabIndex = 12;
            this.upload.Text = "Importer";
            this.upload.UseVisualStyleBackColor = false;
            this.upload.Click += new System.EventHandler(this.upload_Click);
            // 
            // picture
            // 
            this.picture.Location = new System.Drawing.Point(701, 119);
            this.picture.Name = "picture";
            this.picture.Size = new System.Drawing.Size(238, 177);
            this.picture.TabIndex = 13;
            this.picture.TabStop = false;
            // 
            // logo
            // 
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logo.Image = global::FrontEnd_winform_pari.Properties.Resources.WannaBet;
            this.logo.InitialImage = null;
            this.logo.Location = new System.Drawing.Point(0, 0);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(105, 99);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 37;
            this.logo.TabStop = false;
            // 
            // Sports_Actions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 586);
            this.Controls.Add(this.picture);
            this.Controls.Add(this.upload);
            this.Controls.Add(this.add);
            this.Controls.Add(this.filename);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Name = "Sports_Actions";
            this.Text = "Sports_Actions";
            this.Load += new System.EventHandler(this.Sports_Actions_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label tresorerie;
        private System.Windows.Forms.Label article;
        private System.Windows.Forms.Label equipe;
        private System.Windows.Forms.Label Sport;
        private System.Windows.Forms.Label match;
        private System.Windows.Forms.DataGridViewTextBoxColumn Photo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Age;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adresse;
        private System.Windows.Forms.DataGridViewTextBoxColumn Heure;
        private System.Windows.Forms.DataGridViewTextBoxColumn Actions;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox filename;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Button upload;
        private System.Windows.Forms.PictureBox picture;
        private System.Windows.Forms.PictureBox logo;
    }
}