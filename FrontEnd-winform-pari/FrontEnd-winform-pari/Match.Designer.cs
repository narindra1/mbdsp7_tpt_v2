﻿namespace FrontEnd_winform_pari
{
    partial class Match
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.logo = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Sport = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.matchList = new System.Windows.Forms.DataGridView();
            this.ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.team1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.team2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adresse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Heure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.delete = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.matchnonfini = new System.Windows.Forms.DataGridView();
            this.idnon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eqp1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eqp2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matchList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchnonfini)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel1.Controls.Add(this.logo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(5, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 87);
            this.panel1.TabIndex = 3;
            // 
            // logo
            // 
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logo.Image = global::FrontEnd_winform_pari.Properties.Resources.WannaBet;
            this.logo.InitialImage = null;
            this.logo.Location = new System.Drawing.Point(0, -1);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(117, 87);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 51;
            this.logo.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(837, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Admnistrateur";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.Sport);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Location = new System.Drawing.Point(5, 82);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(117, 502);
            this.panel2.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(18, 290);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tableau de bord";
            this.label6.Click += new System.EventHandler(this.Label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(31, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Article";
            this.label5.Click += new System.EventHandler(this.Label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(31, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Equipe";
            this.label4.Click += new System.EventHandler(this.Label4_Click);
            // 
            // Sport
            // 
            this.Sport.AutoSize = true;
            this.Sport.BackColor = System.Drawing.Color.Transparent;
            this.Sport.ForeColor = System.Drawing.SystemColors.Control;
            this.Sport.Location = new System.Drawing.Point(31, 119);
            this.Sport.Name = "Sport";
            this.Sport.Size = new System.Drawing.Size(32, 13);
            this.Sport.TabIndex = 0;
            this.Sport.Text = "Sport";
            this.Sport.Click += new System.EventHandler(this.Sport_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(31, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Match";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // matchList
            // 
            this.matchList.BackgroundColor = System.Drawing.Color.Lavender;
            this.matchList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.matchList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID,
            this.team1,
            this.team2,
            this.Age,
            this.Email,
            this.Adresse,
            this.Heure});
            this.matchList.Location = new System.Drawing.Point(233, 196);
            this.matchList.Name = "matchList";
            this.matchList.Size = new System.Drawing.Size(744, 167);
            this.matchList.TabIndex = 5;
            this.matchList.Text = "dataGridView1";
            this.matchList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.matchList_CellDoubleClick);
            // 
            // ID
            // 
            this.ID.HeaderText = "ID";
            this.ID.Name = "ID";
            this.ID.Visible = false;
            // 
            // team1
            // 
            this.team1.HeaderText = "Equipe 1";
            this.team1.Name = "team1";
            // 
            // team2
            // 
            this.team2.HeaderText = "Equipe 2";
            this.team2.Name = "team2";
            // 
            // Age
            // 
            this.Age.HeaderText = "Score équipe 1";
            this.Age.Name = "Age";
            // 
            // Email
            // 
            this.Email.HeaderText = "Score équipe 2";
            this.Email.Name = "Email";
            // 
            // Adresse
            // 
            this.Adresse.HeaderText = "Date";
            this.Adresse.Name = "Adresse";
            // 
            // Heure
            // 
            this.Heure.HeaderText = "Heure";
            this.Heure.Name = "Heure";
            // 
            // delete
            // 
            this.delete.BackColor = System.Drawing.Color.Firebrick;
            this.delete.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.delete.ForeColor = System.Drawing.Color.White;
            this.delete.Location = new System.Drawing.Point(557, 107);
            this.delete.Name = "delete";
            this.delete.Size = new System.Drawing.Size(173, 33);
            this.delete.TabIndex = 46;
            this.delete.Text = "Supprimer un match";
            this.delete.UseVisualStyleBackColor = false;
            this.delete.Click += new System.EventHandler(this.delete_Click);
            // 
            // add
            // 
            this.add.BackColor = System.Drawing.Color.DarkTurquoise;
            this.add.Location = new System.Drawing.Point(321, 107);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(173, 33);
            this.add.TabIndex = 45;
            this.add.Text = "Ajouter un match";
            this.add.UseVisualStyleBackColor = false;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // matchnonfini
            // 
            this.matchnonfini.BackgroundColor = System.Drawing.Color.Lavender;
            this.matchnonfini.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.matchnonfini.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idnon,
            this.eqp1,
            this.eqp2,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.matchnonfini.Location = new System.Drawing.Point(233, 406);
            this.matchnonfini.Name = "matchnonfini";
            this.matchnonfini.Size = new System.Drawing.Size(544, 105);
            this.matchnonfini.TabIndex = 48;
            this.matchnonfini.Text = "dataGridView1";
            this.matchnonfini.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.matchnonfini_CellDoubleClick);
            // 
            // idnon
            // 
            this.idnon.HeaderText = "ID";
            this.idnon.Name = "idnon";
            this.idnon.Visible = false;
            // 
            // eqp1
            // 
            this.eqp1.HeaderText = "Equipe 1";
            this.eqp1.Name = "eqp1";
            // 
            // eqp2
            // 
            this.eqp2.HeaderText = "Equipe 2";
            this.eqp2.Name = "eqp2";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "Date";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Heure";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(518, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 49;
            this.label3.Text = "Match terminé";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(511, 382);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 13);
            this.label7.TabIndex = 50;
            this.label7.Text = "Match non terminé";
            // 
            // Match
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 585);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.matchnonfini);
            this.Controls.Add(this.delete);
            this.Controls.Add(this.add);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.matchList);
            this.Name = "Match";
            this.Text = "Match";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.matchList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.matchnonfini)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Sport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView matchList;
        private System.Windows.Forms.Button delete;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.DataGridView matchnonfini;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn team1;
        private System.Windows.Forms.DataGridViewTextBoxColumn team2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Age;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adresse;
        private System.Windows.Forms.DataGridViewTextBoxColumn Heure;
        private System.Windows.Forms.DataGridViewTextBoxColumn idnon;
        private System.Windows.Forms.DataGridViewTextBoxColumn eqp1;
        private System.Windows.Forms.DataGridViewTextBoxColumn eqp2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.PictureBox logo;
    }
}