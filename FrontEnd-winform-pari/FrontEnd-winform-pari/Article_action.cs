﻿using FrontEnd_winform_pari.Model;
using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Article_action : Form
    {
        static HttpClient client = new HttpClient();
        byte[] file = null;
        string chemin = "";
        int idforupdate;
        string imageolder;
        bool update = false;

        Articles article = new Articles();
        public Article_action()
        {
            InitializeComponent();
        }

        private void Upload_Click(object sender, EventArgs e)
        {
            try
            {
                (string, byte[]) response = new GlobalService().ChooseFileDialog(picture, filename);
                chemin = response.Item1;
                file = response.Item2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private async void Add_Click_1(object sender, EventArgs e)
        {
            string libelle = titre.Text;
            if (libelle != null && libelle != "")
            {
                article.date = DateTime.Now;
                article.image = filename.Text;
                article.description = description.Text;
                article.titre = titre.Text;
                if (update)
                {
                    //article.Id = this.idforupdate;
                    article.image = filename.Text;
                    //await new ArticleService().UpdateSport(client, article, file, filename.Text, this.imageolder);
                }
                else
                    await new ArticleService().InsertArticle(client, article, file, chemin);
            }
        }

    }
}
