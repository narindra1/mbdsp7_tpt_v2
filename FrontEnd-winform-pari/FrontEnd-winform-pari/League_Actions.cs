﻿using FrontEnd_winform_pari.Model;
using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class League_Actions : Form
    {
        string uri = "https://tptnode.herokuapp.com/static/image/leagues/";
        static HttpClient client = new HttpClient();
        byte[] file = null;
        string chemin = "";
        string imageolder = "";
        bool update = false;
        ObjectPaginate<Sports> sports = null;
        int idforupdate;
        int sport_Id = 0;
        List<League> league = new List<League>();
        public League_Actions(bool updatereceive, string id = "")
        {
            InitializeComponent();
            update = updatereceive;
            Console.WriteLine("variable update = " + update);
            InitComboBox();
            init(id);
        }

        public async void InitComboBox()
        {
            sports = await new SportsService().GetSportsAsync(client);
            foreach (var sport in sports.docs)
            {
                sportlist.Items.Add(sport.Name);
            }
        }

        private void League_Actions_Load(object sender, EventArgs e)
        {

        }

        private void upload_Click(object sender, EventArgs e)
        {
            try
            {
                (string, byte[]) response = new GlobalService().ChooseFileDialog(picture, filename);
                chemin = response.Item1;
                file = response.Item2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async void add_Click(object sender, EventArgs e)
        {
            string libelle = name.Text;
            League league = new League();
            if (libelle != null && libelle != "" && sport_Id != 0)
            {
                league.name = name.Text;
                league.id_sport = sport_Id;

                if (update)
                {
                    Console.WriteLine("add_click -> update");
                    league.id = this.idforupdate;
                    league.image = filename.Text;
                    //Console.WriteLine(" league.id = " + league.id);
                    //Console.WriteLine(" league.name = " + league.name);
                    //Console.WriteLine(" league.id_sport = " + league.id_sport);
                    //Console.WriteLine(" league.image = " + league.image);
                    //MessageBox.Show("update sport");
                    await new LeagueService().updateLeague(client, league, file, chemin, imageolder);
                }
                else
                {
                    if (file != null && chemin != "")
                    {
                        Console.WriteLine("add_click -> insert");
                        await new LeagueService().InsertLeague(client, league, file, chemin);
                    }
                }
            }
        }

        private void sportlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedItem = (string)sportlist.SelectedItem;
            sport_Id = sports.docs.Find(s => s.Name == selectedItem).Id;
            Console.WriteLine("sport_Id = " + sport_Id);
            //MessageBox.Show("Sport Id = " + sport_Id);
        }

        private async void init(string id)
        {
            if (this.update)
            {
                this.idforupdate = Int32.Parse(id);
                this.league = await new LeagueService().getOneLeagueAsync(client, id);
                name.Text = this.league.ElementAt(0).name;
                filename.Text = this.league.ElementAt(0).image;
                picture.ImageLocation = uri + this.league.ElementAt(0).image;
                this.imageolder = this.league.ElementAt(0).image;
                //MessageBox.Show("this.league.ElementAt(0).sport.ElementAt(0).Name = " + this.league.ElementAt(0).sport.ElementAt(0).Name);
                sportlist.SelectedIndex = sportlist.FindStringExact(this.league.ElementAt(0).sport.ElementAt(0).Name);
                sportlist.Refresh();
                picture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }
    }
}
