﻿namespace FrontEnd_winform_pari
{
    partial class Article_action
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.picture = new System.Windows.Forms.PictureBox();
            this.upload = new System.Windows.Forms.Button();
            this.add = new System.Windows.Forms.Button();
            this.titre = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Actions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Heure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adresse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Photo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tresorerie = new System.Windows.Forms.Label();
            this.articles = new System.Windows.Forms.Label();
            this.equipe = new System.Windows.Forms.Label();
            this.Sport = new System.Windows.Forms.Label();
            this.match = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.description = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.filename = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.logo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picture)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(289, 353);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 13);
            this.label4.TabIndex = 25;
            this.label4.Text = "Description";
            // 
            // picture
            // 
            this.picture.Location = new System.Drawing.Point(712, 129);
            this.picture.Name = "picture";
            this.picture.Size = new System.Drawing.Size(238, 177);
            this.picture.TabIndex = 24;
            this.picture.TabStop = false;
            // 
            // upload
            // 
            this.upload.BackColor = System.Drawing.Color.Black;
            this.upload.ForeColor = System.Drawing.Color.White;
            this.upload.Location = new System.Drawing.Point(681, 350);
            this.upload.Name = "upload";
            this.upload.Size = new System.Drawing.Size(102, 28);
            this.upload.TabIndex = 23;
            this.upload.Text = "Importer";
            this.upload.UseVisualStyleBackColor = false;
            this.upload.Click += new System.EventHandler(this.Upload_Click);
            // 
            // add
            // 
            this.add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.add.ForeColor = System.Drawing.Color.White;
            this.add.Location = new System.Drawing.Point(467, 534);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(120, 38);
            this.add.TabIndex = 22;
            this.add.Text = "Enregistrer";
            this.add.UseVisualStyleBackColor = false;
            this.add.Click += new System.EventHandler(this.Add_Click_1);
            // 
            // titre
            // 
            this.titre.Location = new System.Drawing.Point(423, 255);
            this.titre.Multiline = true;
            this.titre.Name = "titre";
            this.titre.Size = new System.Drawing.Size(217, 28);
            this.titre.TabIndex = 19;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(289, 258);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "Titre:";
            // 
            // Actions
            // 
            this.Actions.HeaderText = "Actions";
            this.Actions.MinimumWidth = 6;
            this.Actions.Name = "Actions";
            this.Actions.Width = 125;
            // 
            // Heure
            // 
            this.Heure.HeaderText = "Heure";
            this.Heure.MinimumWidth = 6;
            this.Heure.Name = "Heure";
            this.Heure.Width = 125;
            // 
            // Adresse
            // 
            this.Adresse.HeaderText = "Date";
            this.Adresse.MinimumWidth = 6;
            this.Adresse.Name = "Adresse";
            this.Adresse.Width = 125;
            // 
            // Email
            // 
            this.Email.HeaderText = "Score équipe 2";
            this.Email.MinimumWidth = 6;
            this.Email.Name = "Email";
            this.Email.Width = 125;
            // 
            // Age
            // 
            this.Age.HeaderText = "Score équipe 1";
            this.Age.MinimumWidth = 6;
            this.Age.Name = "Age";
            this.Age.Width = 125;
            // 
            // Nom
            // 
            this.Nom.HeaderText = "Equipe 2";
            this.Nom.MinimumWidth = 6;
            this.Nom.Name = "Nom";
            this.Nom.Width = 125;
            // 
            // Photo
            // 
            this.Photo.HeaderText = "Equipe 1";
            this.Photo.MinimumWidth = 6;
            this.Photo.Name = "Photo";
            this.Photo.Width = 125;
            // 
            // tresorerie
            // 
            this.tresorerie.AutoSize = true;
            this.tresorerie.ForeColor = System.Drawing.Color.White;
            this.tresorerie.Location = new System.Drawing.Point(20, 280);
            this.tresorerie.Name = "tresorerie";
            this.tresorerie.Size = new System.Drawing.Size(54, 13);
            this.tresorerie.TabIndex = 0;
            this.tresorerie.Text = "Trésorerie";
            // 
            // articles
            // 
            this.articles.AutoSize = true;
            this.articles.ForeColor = System.Drawing.Color.White;
            this.articles.Location = new System.Drawing.Point(20, 221);
            this.articles.Name = "articles";
            this.articles.Size = new System.Drawing.Size(36, 13);
            this.articles.TabIndex = 0;
            this.articles.Text = "Article";
            // 
            // equipe
            // 
            this.equipe.AutoSize = true;
            this.equipe.ForeColor = System.Drawing.Color.White;
            this.equipe.Location = new System.Drawing.Point(20, 166);
            this.equipe.Name = "equipe";
            this.equipe.Size = new System.Drawing.Size(40, 13);
            this.equipe.TabIndex = 0;
            this.equipe.Text = "Equipe";
            // 
            // Sport
            // 
            this.Sport.AutoSize = true;
            this.Sport.ForeColor = System.Drawing.SystemColors.Window;
            this.Sport.Location = new System.Drawing.Point(20, 109);
            this.Sport.Name = "Sport";
            this.Sport.Size = new System.Drawing.Size(32, 13);
            this.Sport.TabIndex = 0;
            this.Sport.Text = "Sport";
            // 
            // match
            // 
            this.match.AutoSize = true;
            this.match.ForeColor = System.Drawing.Color.White;
            this.match.Location = new System.Drawing.Point(20, 50);
            this.match.Name = "match";
            this.match.Size = new System.Drawing.Size(37, 13);
            this.match.TabIndex = 0;
            this.match.Text = "Match";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel2.Controls.Add(this.tresorerie);
            this.panel2.Controls.Add(this.articles);
            this.panel2.Controls.Add(this.equipe);
            this.panel2.Controls.Add(this.Sport);
            this.panel2.Controls.Add(this.match);
            this.panel2.Location = new System.Drawing.Point(12, 96);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(126, 499);
            this.panel2.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(826, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Admnistrateur";
            // 
            // description
            // 
            this.description.Location = new System.Drawing.Point(423, 350);
            this.description.Multiline = true;
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(217, 28);
            this.description.TabIndex = 26;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel1.Controls.Add(this.logo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 87);
            this.panel1.TabIndex = 16;
            // 
            // filename
            // 
            this.filename.Location = new System.Drawing.Point(801, 350);
            this.filename.Multiline = true;
            this.filename.Name = "filename";
            this.filename.Size = new System.Drawing.Size(217, 28);
            this.filename.TabIndex = 28;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(798, 325);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Image :";
            // 
            // logo
            // 
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logo.Image = global::FrontEnd_winform_pari.Properties.Resources.WannaBet;
            this.logo.InitialImage = null;
            this.logo.Location = new System.Drawing.Point(0, 0);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(126, 87);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 37;
            this.logo.TabStop = false;
            // 
            // Article_action
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 602);
            this.Controls.Add(this.filename);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.picture);
            this.Controls.Add(this.upload);
            this.Controls.Add(this.add);
            this.Controls.Add(this.titre);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.description);
            this.Controls.Add(this.panel1);
            this.Name = "Article_action";
            this.Text = "Article_action";
            ((System.ComponentModel.ISupportInitialize)(this.picture)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox picture;
        private System.Windows.Forms.Button upload;
        private System.Windows.Forms.Button add;
        private System.Windows.Forms.TextBox titre;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Actions;
        private System.Windows.Forms.DataGridViewTextBoxColumn Heure;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adresse;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Age;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Photo;
        private System.Windows.Forms.Label tresorerie;
        private System.Windows.Forms.Label articles;
        private System.Windows.Forms.Label equipe;
        private System.Windows.Forms.Label Sport;
        private System.Windows.Forms.Label match;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox description;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox filename;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox logo;
    }
}