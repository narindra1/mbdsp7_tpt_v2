﻿namespace FrontEnd_winform_pari
{
    partial class Match_Actions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.add = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Actions = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Heure = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adresse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Age = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Photo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tresorerie = new System.Windows.Forms.Label();
            this.article = new System.Windows.Forms.Label();
            this.equipe = new System.Windows.Forms.Label();
            this.Sport = new System.Windows.Forms.Label();
            this.match = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.logo = new System.Windows.Forms.PictureBox();
            this.team_one_list = new System.Windows.Forms.ComboBox();
            this.team_list_two = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.timematch = new System.Windows.Forms.DateTimePicker();
            this.datematch = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.liguelist = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.sportlist = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.quote1 = new System.Windows.Forms.TextBox();
            this.quote2 = new System.Windows.Forms.TextBox();
            this.quotenull = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // add
            // 
            this.add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.add.ForeColor = System.Drawing.Color.White;
            this.add.Location = new System.Drawing.Point(381, 459);
            this.add.Name = "add";
            this.add.Size = new System.Drawing.Size(120, 38);
            this.add.TabIndex = 20;
            this.add.Text = "Enregistrer";
            this.add.UseVisualStyleBackColor = false;
            this.add.Click += new System.EventHandler(this.add_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(215, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Première équipe :";
            // 
            // Actions
            // 
            this.Actions.HeaderText = "Actions";
            this.Actions.MinimumWidth = 6;
            this.Actions.Name = "Actions";
            this.Actions.Width = 125;
            // 
            // Heure
            // 
            this.Heure.HeaderText = "Heure";
            this.Heure.MinimumWidth = 6;
            this.Heure.Name = "Heure";
            this.Heure.Width = 125;
            // 
            // Adresse
            // 
            this.Adresse.HeaderText = "Date";
            this.Adresse.MinimumWidth = 6;
            this.Adresse.Name = "Adresse";
            this.Adresse.Width = 125;
            // 
            // Email
            // 
            this.Email.HeaderText = "Score équipe 2";
            this.Email.MinimumWidth = 6;
            this.Email.Name = "Email";
            this.Email.Width = 125;
            // 
            // Age
            // 
            this.Age.HeaderText = "Score équipe 1";
            this.Age.MinimumWidth = 6;
            this.Age.Name = "Age";
            this.Age.Width = 125;
            // 
            // Nom
            // 
            this.Nom.HeaderText = "Equipe 2";
            this.Nom.MinimumWidth = 6;
            this.Nom.Name = "Nom";
            this.Nom.Width = 125;
            // 
            // Photo
            // 
            this.Photo.HeaderText = "Equipe 1";
            this.Photo.MinimumWidth = 6;
            this.Photo.Name = "Photo";
            this.Photo.Width = 125;
            // 
            // tresorerie
            // 
            this.tresorerie.AutoSize = true;
            this.tresorerie.ForeColor = System.Drawing.Color.White;
            this.tresorerie.Location = new System.Drawing.Point(20, 280);
            this.tresorerie.Name = "tresorerie";
            this.tresorerie.Size = new System.Drawing.Size(54, 13);
            this.tresorerie.TabIndex = 0;
            this.tresorerie.Text = "Trésorerie";
            // 
            // article
            // 
            this.article.AutoSize = true;
            this.article.ForeColor = System.Drawing.Color.White;
            this.article.Location = new System.Drawing.Point(20, 221);
            this.article.Name = "article";
            this.article.Size = new System.Drawing.Size(36, 13);
            this.article.TabIndex = 0;
            this.article.Text = "Article";
            // 
            // equipe
            // 
            this.equipe.AutoSize = true;
            this.equipe.ForeColor = System.Drawing.Color.White;
            this.equipe.Location = new System.Drawing.Point(20, 166);
            this.equipe.Name = "equipe";
            this.equipe.Size = new System.Drawing.Size(40, 13);
            this.equipe.TabIndex = 0;
            this.equipe.Text = "Equipe";
            // 
            // Sport
            // 
            this.Sport.AutoSize = true;
            this.Sport.ForeColor = System.Drawing.SystemColors.Window;
            this.Sport.Location = new System.Drawing.Point(20, 109);
            this.Sport.Name = "Sport";
            this.Sport.Size = new System.Drawing.Size(32, 13);
            this.Sport.TabIndex = 0;
            this.Sport.Text = "Sport";
            // 
            // match
            // 
            this.match.AutoSize = true;
            this.match.ForeColor = System.Drawing.Color.White;
            this.match.Location = new System.Drawing.Point(20, 50);
            this.match.Name = "match";
            this.match.Size = new System.Drawing.Size(37, 13);
            this.match.TabIndex = 0;
            this.match.Text = "Match";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel2.Controls.Add(this.tresorerie);
            this.panel2.Controls.Add(this.article);
            this.panel2.Controls.Add(this.equipe);
            this.panel2.Controls.Add(this.Sport);
            this.panel2.Controls.Add(this.match);
            this.panel2.Location = new System.Drawing.Point(0, 85);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(136, 500);
            this.panel2.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(826, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Admnistrateur";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel1.Controls.Add(this.logo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 87);
            this.panel1.TabIndex = 14;
            // 
            // logo
            // 
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logo.Image = global::FrontEnd_winform_pari.Properties.Resources.WannaBet;
            this.logo.InitialImage = null;
            this.logo.Location = new System.Drawing.Point(0, 0);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(136, 87);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 37;
            this.logo.TabStop = false;
            // 
            // team_one_list
            // 
            this.team_one_list.FormattingEnabled = true;
            this.team_one_list.Location = new System.Drawing.Point(349, 238);
            this.team_one_list.Name = "team_one_list";
            this.team_one_list.Size = new System.Drawing.Size(217, 21);
            this.team_one_list.TabIndex = 22;
            this.team_one_list.SelectedIndexChanged += new System.EventHandler(this.team_one_list_SelectedIndexChanged);
            // 
            // team_list_two
            // 
            this.team_list_two.FormattingEnabled = true;
            this.team_list_two.Location = new System.Drawing.Point(349, 296);
            this.team_list_two.Name = "team_list_two";
            this.team_list_two.Size = new System.Drawing.Size(217, 21);
            this.team_list_two.TabIndex = 24;
            this.team_list_two.SelectedIndexChanged += new System.EventHandler(this.team_list_two_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(215, 299);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Deuxième équipe :";
            // 
            // timematch
            // 
            this.timematch.CustomFormat = "";
            this.timematch.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timematch.Location = new System.Drawing.Point(598, 369);
            this.timematch.Name = "timematch";
            this.timematch.ShowUpDown = true;
            this.timematch.Size = new System.Drawing.Size(116, 20);
            this.timematch.TabIndex = 27;
            // 
            // datematch
            // 
            this.datematch.Checked = false;
            this.datematch.Location = new System.Drawing.Point(349, 369);
            this.datematch.Name = "datematch";
            this.datematch.Size = new System.Drawing.Size(217, 20);
            this.datematch.TabIndex = 25;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(215, 375);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Date: ";
            // 
            // liguelist
            // 
            this.liguelist.FormattingEnabled = true;
            this.liguelist.Location = new System.Drawing.Point(349, 183);
            this.liguelist.Name = "liguelist";
            this.liguelist.Size = new System.Drawing.Size(217, 21);
            this.liguelist.TabIndex = 30;
            this.liguelist.SelectedIndexChanged += new System.EventHandler(this.leaguelist_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(215, 183);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 29;
            this.label6.Text = "Ligue :";
            // 
            // sportlist
            // 
            this.sportlist.FormattingEnabled = true;
            this.sportlist.Location = new System.Drawing.Point(349, 126);
            this.sportlist.Name = "sportlist";
            this.sportlist.Size = new System.Drawing.Size(217, 21);
            this.sportlist.TabIndex = 28;
            this.sportlist.SelectedIndexChanged += new System.EventHandler(this.sportlist_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(215, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Sport :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(600, 299);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "Quote Deuxième équipe :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(600, 242);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 13);
            this.label8.TabIndex = 31;
            this.label8.Text = "Quote Première équipe :";
            // 
            // quote1
            // 
            this.quote1.Location = new System.Drawing.Point(744, 239);
            this.quote1.Name = "quote1";
            this.quote1.Size = new System.Drawing.Size(100, 20);
            this.quote1.TabIndex = 34;
            // 
            // quote2
            // 
            this.quote2.Location = new System.Drawing.Point(744, 299);
            this.quote2.Name = "quote2";
            this.quote2.Size = new System.Drawing.Size(100, 20);
            this.quote2.TabIndex = 35;
            // 
            // quotenull
            // 
            this.quotenull.Location = new System.Drawing.Point(744, 187);
            this.quotenull.Name = "quotenull";
            this.quotenull.Size = new System.Drawing.Size(100, 20);
            this.quotenull.TabIndex = 37;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(600, 190);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 36;
            this.label9.Text = "Quote match null :";
            // 
            // Match_Actions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 586);
            this.Controls.Add(this.quotenull);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.quote2);
            this.Controls.Add(this.quote1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.liguelist);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.sportlist);
            this.Controls.Add(this.timematch);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.datematch);
            this.Controls.Add(this.team_list_two);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.team_one_list);
            this.Controls.Add(this.add);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Match_Actions";
            this.Text = "Match_Actions";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button add;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Actions;
        private System.Windows.Forms.DataGridViewTextBoxColumn Heure;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adresse;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email;
        private System.Windows.Forms.DataGridViewTextBoxColumn Age;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Photo;
        private System.Windows.Forms.Label tresorerie;
        private System.Windows.Forms.Label article;
        private System.Windows.Forms.Label equipe;
        private System.Windows.Forms.Label Sport;
        private System.Windows.Forms.Label match;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox team_one_list;
        private System.Windows.Forms.ComboBox team_list_two;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker timematch;
        private System.Windows.Forms.DateTimePicker datematch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox liguelist;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox sportlist;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox quote1;
        private System.Windows.Forms.TextBox quote2;
        private System.Windows.Forms.TextBox quotenull;
        private System.Windows.Forms.Label label9;
    }
}