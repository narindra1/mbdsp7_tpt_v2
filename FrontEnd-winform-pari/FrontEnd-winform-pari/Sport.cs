using FrontEnd_winform_pari.Model;
using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Sport : Form
    {
        static HttpClient client = new HttpClient();
        string cellclicked = "";
        bool update = false;
        public Sport()
        {
            InitializeComponent();
            InitGrid();
        }

        public async void InitGrid()
        {
            WebClient wc = new WebClient();
            var sports = await new SportsService().GetSportsAsync(client);
            foreach (var sport in sports.docs)
            {
                try
                {
                    if (wc.DownloadData("https://tptnode.herokuapp.com/static/image/sports/" + sport.image) != null)
                    {
                        byte[] bytes = wc.DownloadData("https://tptnode.herokuapp.com/static/image/sports/" + sport.image);
                        MemoryStream ms = new MemoryStream(bytes);
                        Image img = Image.FromStream(ms);
                        sary.ImageLayout = DataGridViewImageCellLayout.Stretch;
                        sportslist.RowTemplate.Height = 80;
                        sportslist.Rows.Add(sport.Id, sport.Name, img);
                    }
                }
                catch (WebException wex)
                {
                    if (((HttpWebResponse)wex.Response).StatusCode == HttpStatusCode.NotFound)
                    {
                        //sportslist.RowTemplate.Height = 80;
                        sportslist.Rows.Add(sport.Id, sport.Name);
                    }
                }
            }
        }

        public void ResetGrid()
        {
            sportslist.Rows.Clear();
            InitGrid();
        }

        private void Sport_Load(object sender, EventArgs e)
        {

        }

        private void add_Click(object sender, EventArgs e)
        {
            update = false;
            new Sports_Actions(update).Show();
        }

        private async void delete_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Etes-vous sur de supprimer ?",
                                      "Suppression confirmée",
                                      MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                await new SportsService().DeleteSportsAsync(cellclicked, client);
                MessageBox.Show("Le sport a été supprimer avec success !!! ", "Suppression", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ResetGrid();
            }
        }

        private void edit_Click(object sender, EventArgs e)
        {
            update = true;
            new Sports_Actions(update, cellclicked).Show();
        }

        private void sportslist_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.sportslist.Rows[e.RowIndex];
                cellclicked = row.Cells["ID"].Value.ToString();
                string nameclicked = row.Cells["Libelle"].Value.ToString();

                MessageBox.Show("Vous avez sélectionné " + nameclicked);
            }
        }

        private void Label20_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Match().Show();
        }

        private void Label19_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Users().Show();
        }

        private void Label18_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Equipes().Show();
        }

        private void Label17_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Article().Show();
        }

        private void Label16_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Dashboard().Show();
        }
    }
}
