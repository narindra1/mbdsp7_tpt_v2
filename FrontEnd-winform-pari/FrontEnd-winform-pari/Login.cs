﻿using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Login : Form
    {
        static HttpClient client = new HttpClient();
        public Login()
        {
            InitializeComponent();
        }

        private async void connexion_ClickAsync(object sender, EventArgs e)
        {
            string utilisateur = username.Text;
            string motdepasse = password.Text;

           await new UserService().Login(this,client, utilisateur, motdepasse);

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}
