﻿using FrontEnd_winform_pari.Model;
using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Match_Actions : Form
    {
        string uri = "https://tptnode.herokuapp.com/static/image/matchs/";
        bool update = false;
        List<Matchs> matchs = new List<Matchs>();
        HttpClient client = new HttpClient();
        ObjectPaginate<Sports> sports = null;
        ObjectPaginate<League> leagues = null;
        int sport_Id = 0;
        int league_Id = 0;
        int idforupdate = 0;
        ObjectPaginate<Teams> teams = null;
        int team1_Id = 0;
        int team2_Id = 0;
        Form parent;
        public Match_Actions(bool updatereceive, Form match,string id="")
        {
            parent = match;
            InitializeComponent();
            update = updatereceive;
            InitComboBoxSport();
            InitComboBoxTeam();
        }

        public async void InitComboBoxSport()
        {
            sports = await new SportsService().GetSportsAsync(client);
            foreach (var sport in sports.docs)
            {
                sportlist.Items.Add(sport.Name);
            }
            leagues = await new LeagueService().GetLeaguesAsync(client);
            foreach (var lig in leagues.docs)
            {
                liguelist.Items.Add(lig.name);
            }
        }

        public async void InitComboBoxTeam()
        {
            teams = await new TeamsService().GetTeamsAsync(client);
            foreach (var team in teams.docs)
            {
                team_one_list.Items.Add(team.Name);
                team_list_two.Items.Add(team.Name);
            }
        }

        private async void add_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime date = datematch.Value;
                string time = (timematch.Value).ToString("hh:mm tt");
                if (date != null && time != null && time != "" && sport_Id != 0 && team1_Id != 0 && team2_Id != 0)
                {
                    Matchs match = new Matchs();
                    match.team1_Id = team1_Id;
                    match.team2_Id = team2_Id;
                    match.Date_time = date;
                    match.Date = date;
                    match.Time = time;
                    match.quote_1 = Int32.Parse(quote1.Text);
                    match.quote_2 = Int32.Parse(quote2.Text);
                    match.quote_null = Int32.Parse(quotenull.Text);
                    //match.Score_1 = 0;
                    //  match.Score_2 = 0;
                    match.Team_1 = new List<Teams>();
                    match.Team_2 = new List<Teams>();

                    //MessageBox.Show(""+ match.team1_Id+"test" + match.team2_Id);
                    await new MatchService().InsertMatch(client, match);
                    parent.Hide();
                    this.Hide();
                    new Match().Show();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("il y a un erreur lors de l ajout");
            }
        }

        private void sportlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedItem = (string)sportlist.SelectedItem;
            sport_Id = sports.docs.Find(s => s.Name == selectedItem).Id;
            //MessageBox.Show("Sport Id = " + sport_Id);
        }
        private void leaguelist_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedItem = (string)liguelist.SelectedItem;
            league_Id = leagues.docs.Find(s => s.name == selectedItem).id;
            //MessageBox.Show("Sport Id = " + sport_Id);
        }
        private void team_one_list_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedItem = (string)team_one_list.SelectedItem;
            team1_Id = teams.docs.Find(s => s.Name == selectedItem).Id;
            //MessageBox.Show("Match 1 Id = " + team1_Id);
        }

        private void team_list_two_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedItem = (string)team_list_two.SelectedItem;
            team2_Id = teams.docs.Find(s => s.Name == selectedItem).Id;
            //MessageBox.Show("Match 2 Id = " + team2_Id);
        }

        private async void init(string id)
        {
            if (update)
            {
                this.idforupdate = Int32.Parse(id);
                this.matchs = await new MatchService().getOneMatchAsync(client, id);
                //sportlist.SelectedIndex = this.matchs.ElementAt(0).
            }
        }
    }
}
