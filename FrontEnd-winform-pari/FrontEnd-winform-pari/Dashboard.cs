﻿using FrontEnd_winform_pari.Model;
using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace FrontEnd_winform_pari
{
    public partial class Dashboard : Form
    {

        private HttpClient client = new HttpClient();
        ToolTip tooltip = new ToolTip();
        Point? prevPosition = null;
        public Dashboard()
        {
            try
            {
                InitializeComponent(); Init(); chart(); chartparmois(2021);
            }
            catch (Exception e) {
                MessageBox.Show("il y a une erreur,veiller contacter l'admin");
            }
        }
        public async void Init()
        {
            var dashboards = await new DashboardService().GetDashboardAsync(client);
            debits.Text = dashboards.debit.ToString();
            credits.Text = dashboards.credit.ToString();
            soldes.Text = dashboards.solde.ToString();
            //var totals = await new DashboardService().GetTotalBetAsync(client);
            //total.Text = totals.ToString();

        }
        public async void chart() {
            try
            {
                var dashboards = await new DashboardService().ChartMatch(client);
                foreach (var dashboard in dashboards)
                {
                    chart1.Series["Match"].Points.AddXY(dashboard.match[0].Team_1[0].Name + " vs " + dashboard.match[0].Team_2[0].Name, dashboard.count);
                }
                //chart title  
                chart1.Titles.Add("Nombre de paris par match");
                chart1.ChartAreas[0].Area3DStyle.Enable3D = true;

                var teamchart = await new DashboardService().ChartTeam(client);
                foreach (var dashboard in teamchart)
                {
                    if (dashboard.team_detail.Count == 1)
                    {
                        chart2.Series["Match"].Points.AddXY(dashboard.team_detail[0].Name, dashboard.count);
                    }
                }
                chart2.Series[0].ChartType = SeriesChartType.Pie;
                chart2.Titles.Add("Nombre de paris par equipe");
                chart2.ChartAreas[0].Area3DStyle.Enable3D = true;
            }
            catch (Exception e)
            {
                MessageBox.Show("connexion interompue");
            }
        }
        public async void chartparmois(int year)
        {
            try { 
                int totals = 0;
                chart3.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                chart3.Series[0].ChartType = SeriesChartType.Line;
                var dashmois = await new DashboardService().ChartParMois(client, year);
                String[] mois = new string[12] { "jan", "fév", "mar", "av", "mai", "jui", "jui", "août", "sept", "oct", "nov", "dec" };
                Int32[] valeur = new Int32[12] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
  
                foreach (var parm in dashmois)
                {
                    totals += Convert.ToInt32(parm[1]);
                    for (int i = 0; i < 12; i++) {
                        if (Convert.ToInt32(parm[0]) == i+1)
                        {
                            valeur[i] = Convert.ToInt32(parm[1]);
                        }
                    }
                }
                for (int i = 0; i < valeur.Length; i++) {
                    chart3.Series["Match"].Points.AddXY(mois[i], valeur[i]);
                }
                chart3.Refresh();
                total.Text = totals.ToString();
                years.Text = year.ToString();
                years2.Text = year.ToString();
            }
            catch (Exception e) {
                MessageBox.Show("il y a une erreur,veiller contacter l'admin");
            }

}

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            chart3.Series.Clear();
            chart3.Series.Add("Match");

            chartparmois(Int32.Parse(comboBox1.Text));
        }

        private void Label20_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Match().Show();
        }

        private void Label19_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Users().Show();
        }

        private void Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Label18_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Equipes().Show();
        }

        private void Label17_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Article().Show();
        }
    }
}
