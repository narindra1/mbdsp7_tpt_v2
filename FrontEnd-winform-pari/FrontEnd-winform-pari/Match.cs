﻿using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Match : Form
    {
        static HttpClient client = new HttpClient();
        bool update = false;
        string cellclicked = "";
        public Match()
        {
            InitializeComponent();
            InitGrid();
        }

        public async void InitGrid()
        {
            var matchs = await new MatchService().GetMatchsAsync(client);
            foreach (var match in matchs.docs)
            {
                matchList.Rows.Add(match.Id, match.Team_1.ElementAt(0).Name, match.Team_2.ElementAt(0).Name, match.Score_1, match.Score_2, match.Date.ToString("yyyy-MM-dd") , match.Time);
            }
            var matchsnon = await new MatchService().GetMatchsDontAsync(client);
            foreach (var match in matchsnon.docs)
            {
                matchnonfini.Rows.Add(match.Id, match.Team_1.ElementAt(0).Name, match.Team_2.ElementAt(0).Name, match.Date.ToString("yyyy-MM-dd"), match.Time);
            }
        }

        public void ResetGrid()
        {
            matchList.Rows.Clear();
            matchnonfini.Rows.Clear();
            InitGrid();
        }

        private void add_Click(object sender, EventArgs e)
        {
            update = false;
            new Match_Actions(update,this).Show();
        }

        private void edit_Click(object sender, EventArgs e)
        {
            update = true;
            new Match_Actions(update, this, cellclicked).Show();
        }

        private void matchList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.matchList.Rows[e.RowIndex];
                cellclicked = row.Cells["ID"].Value.ToString();
                string nameclicked1 = row.Cells["team1"].Value.ToString();
                string nameclicked2 = row.Cells["team2"].Value.ToString();

                MessageBox.Show("Vous avez sélectionné le match entre " + nameclicked1 + " et " + nameclicked2);
            }
        }
        private void matchnonfini_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.matchnonfini.Rows[e.RowIndex];
                cellclicked = row.Cells["idnon"].Value.ToString();
                string nameclicked1 = row.Cells["eqp1"].Value.ToString();
                string nameclicked2 = row.Cells["eqp2"].Value.ToString();
                new Finalizmatch(Int32.Parse(cellclicked), nameclicked1, nameclicked2,this).Show();
                MessageBox.Show("Vous avez sélectionné le match entre " + nameclicked1 + " et " + nameclicked2);
            }
        }
        private async void delete_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Etes-vous sur de supprimer ?",
                                      "Suppression confirmée",
                                      MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                await new MatchService().DeleteMatchAsync(cellclicked, client);
                MessageBox.Show("Le sport a été supprimer avec success !!! ", "Suppression", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ResetGrid();
            }
        }

        private void Sport_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Sport().Show();
        }

        private void Label4_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Equipes().Show();
        }

        private void Label5_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Article().Show();
        }


        private void Label6_Click(object sender, EventArgs e)
        {
            new Dashboard().Show();
            this.Hide(); 
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }
    }
}
