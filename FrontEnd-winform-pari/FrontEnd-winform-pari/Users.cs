﻿using FrontEnd_winform_pari.Model;
using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net.Http;
using System.Text;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Users : Form
    {
        static HttpClient client = new HttpClient();
        public Users()
        {
            InitializeComponent();
            InitGrid();
        }
        public async void InitGrid()
        {

            logo.SizeMode = PictureBoxSizeMode.StretchImage;
            var users = await new UserService().GetUsers(client);
            foreach (User user in users.docs)
            {
                userGrid.Rows.Add(user.username,user.email, user.name, user.address,user.birthday.ToString("yyyy-MM-dd"), user.isAdmin, user.isEnable);
            }
        }
        private void Sport_Click(object sender, EventArgs e)
        {
            new Sport().Show();
        }

        private void matchs_Click(object sender, EventArgs e)
        {
            new Match().Show();
        }

        private void equipe_Click(object sender, EventArgs e)
        {
            new Equipes().Show();
        }

        private void league_Click(object sender, EventArgs e)
        {
            new Leagues().Show();
        }

        private void UserGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void article_Click(object sender, EventArgs e)
        {

            new Article().Show();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Login().Show();
        }

        private void Label6_Click(object sender, EventArgs e)
        {

            new Dashboard().Show();
        }
    }
}
