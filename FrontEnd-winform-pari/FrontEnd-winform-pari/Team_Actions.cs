﻿using FrontEnd_winform_pari.Model;
using FrontEnd_winform_pari.Service;
using FrontEnd_winform_pari.env;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Team_Actions : Form
    {
        HttpClient client = new HttpClient();
        bool update = false;
        ObjectPaginate<League> leagues = null;
        int ligue_Id = 0;
        int idforupdate;
        string imageolder;
        byte[] file = null;
        string chemin = "";
        List<Teams> team = new List<Teams>();

        public Team_Actions(bool updatereceive, string id = "")
        {
            InitializeComponent();
            update = updatereceive;
            InitComboBox();
            init(id);
            //liguelist.Refresh();
        }

        private async void InitComboBox()
        {
            leagues = await new LeagueService().GetLeaguesAsync(client);

            foreach(var league in leagues.docs)
            {
                liguelist.Items.Add(league.name);
            }
            liguelist.Refresh();
        }

        private void Team_Actions_Load(object sender, EventArgs e)
        {

        }

        private void liguelist_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedItem = (string)liguelist.SelectedItem;
            ligue_Id = leagues.docs.Find(s => s.name == selectedItem).id;
            //MessageBox.Show("Sport Id = " + ligue_Id);
        }

        private void upload_Click(object sender, EventArgs e)
        {
            try
            {
                (string, byte[]) response = new GlobalService().ChooseFileDialog(picture, filename);
                chemin = response.Item1;
                file = response.Item2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async void add_Click(object sender, EventArgs e)
        {
            string libelle = name.Text;
            Teams teams = new Teams();
            if (libelle != null && libelle != "" && ligue_Id != 0)
            {
                teams.Name = name.Text;
                teams.id_league = ligue_Id;

                if (update)
                {
                    teams.Id = this.idforupdate;
                    teams.Logo = filename.Text;
                    await new TeamsService().UpdateTeam(client, teams, file, chemin, imageolder);
                    //MessageBox.Show("update sport");
                }
                else
                {
                    if(file != null && chemin != "") await new TeamsService().InserTeam(client, teams, file, chemin);
                }
            }
        }

        private async void init(string id)
        {
            if (this.update)
            {
                this.idforupdate = Int32.Parse(id);
                string uri = api.getUrlImagePathStatic("leagues");
                this.team = await new TeamsService().getOneTeamAsync(client, id);
                name.Text = this.team.ElementAt(0).Name;
                filename.Text = this.team.ElementAt(0).Logo;
                picture.ImageLocation = uri + this.team.ElementAt(0).Logo;
                this.imageolder = this.team.ElementAt(0).Logo;
                //MessageBox.Show(this.team.ElementAt(0).league.name);
                liguelist.SelectedIndex = liguelist.FindString(this.team.ElementAt(0).league.name);
                picture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void Match_Click(object sender, EventArgs e)
        {

        }
    }
}
