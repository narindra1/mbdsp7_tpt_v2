﻿using FrontEnd_winform_pari.Model;
using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Sports_Actions : Form
    {
        static HttpClient client = new HttpClient();
        byte[] file = null;
        string chemin = "";
        int idforupdate;
        string imageolder;
        bool update = false;
        Sports sport = new Sports();

        public Sports_Actions(bool updatereceive, string id = "0")
        {
            InitializeComponent();
            name.Text = sport.Name;
            update = updatereceive;
            init(id);
        }

        private void Sports_Actions_Load(object sender, EventArgs e)
        {

        }

        private void upload_Click(object sender, EventArgs e)
        {
            try
            {
                (string, byte[]) response = new GlobalService().ChooseFileDialog(picture, filename);
                chemin = response.Item1;
                file = response.Item2;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async void add_Click(object sender, EventArgs e)
        {
            string libelle = name.Text;
            Sports sports = new Sports();
            if (libelle != null && libelle != "")
            {
                sports.Name = name.Text;
                if (update)
                {
                    sports.Id = this.idforupdate;
                    sports.image = filename.Text;
                    await new SportsService().UpdateSport(client, sports, file, filename.Text, this.imageolder);
                }
                else
                    await new SportsService().InsertSport(client, sports, file, chemin);
            }
        }

        public async void init(string id)
        {
            if (this.update)
            {
                this.idforupdate = Int32.Parse(id);
                string uri = "https://tptnode.herokuapp.com/static/image/sports/";
                this.sport = await new SportsService().getOneSportAsync(client, id);
                name.Text = this.sport.Name;
                filename.Text = this.sport.image;
                picture.ImageLocation = uri + this.sport.image;
                picture.SizeMode = PictureBoxSizeMode.StretchImage;
                this.imageolder = this.sport.image;
            }
        }

        private void Label2_Click(object sender, EventArgs e)
        {

        }
    }
}
