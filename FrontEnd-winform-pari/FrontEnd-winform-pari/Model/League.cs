﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.Model
{
    public class League
    {
        public int id { get; set; }
        public int id_sport { get; set; }
        public List<Sports> sport { get; set; } 
        public List<Teams> team { get; set; } 
        public string image { get; set; }
        public string name { get; set; }
    }
}
