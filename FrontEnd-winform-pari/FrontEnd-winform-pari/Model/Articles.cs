﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.Model
{
    class Articles
    {
        public int id { get; set; }
        public  String image { get; set; }
        public DateTime date { get; set; }
        public String description { get; set; }
        public String titre { get; set; }
    } 
}
