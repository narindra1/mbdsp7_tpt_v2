﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.Model
{
    class Dashboards
    {

        public Double debit { get; set; }
        public Double credit { get; set; }
        public Double solde { get; set; }
        public int parisTotal { get; set; }
    }
}
