﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.Model
{
    class MatchChart
    {

        public int idmatch { get; set; }
        public int count { get; set; }
        public List<Matchs> match { get; set; }
    }
}
