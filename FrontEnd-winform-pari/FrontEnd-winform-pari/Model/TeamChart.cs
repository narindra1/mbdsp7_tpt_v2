﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.Model
{
    class TeamChart
    {

        public int count { get; set; }
        public int team { get; set; }
        public List<Teams> team_detail { get; set; }
    }
}
