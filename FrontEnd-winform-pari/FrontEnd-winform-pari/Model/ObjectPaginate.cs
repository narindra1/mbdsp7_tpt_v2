﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.Model
{
    public class ObjectPaginate<T>
    {
        public List<T> docs { get; set; }
        public int totalDocs { get; set; }
        public int limit { get; set; }
        public int page { get; set; }
        public int totalPages { get; set; }
        public int pagingCounter { get; set; }
        public Boolean hasPrevPage { get; set; }
        public Boolean hasNextPage { get; set; }
        public string prevPage { get; set; }
        public string nextPage { get; set; }

        public ObjectPaginate()
        {
            this.docs = new List<T>();
        }
    }
}
