﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.Model
{
    class User
    {
        public int Id { get; set; }
        public String username { get; set; }
        public String password { get; set; }
        public String email { get; set; }
        public String name { get; set; }
        public String address { get; set; }
        public DateTime birthday { get; set; }
        public Boolean isAdmin { get; set; }
        public Boolean isEnable { get; set; }
    }
}
