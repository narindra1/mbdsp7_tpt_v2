﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.Model
{
    public class Teams
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public int id_league { get; set; }
        public League league { get; set; }
        public List<Sports> sport { get; set; }
    }
}
