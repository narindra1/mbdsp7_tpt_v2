﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.Model
{
    public class Matchs
    {
        public int Id { get; set; }
        public List<Teams> Team_1 { get; set; }
        public List<Teams> Team_2 { get; set; }
        public int team1_Id { get; set; }
        public int team2_Id { get; set; }
        public Double quote_1 { get; set; }
        public Double quote_2 { get; set; }
        public Double quote_null { get; set; }
        public int? Score_1 { get; set; }
        public int? Score_2 { get; set; }
        public DateTime Date_time { get; set; }
        public DateTime Date { get; set; }
        public string Time { get; set; }
    }
}
