﻿using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Equipes : Form
    {
        readonly static HttpClient client = new HttpClient();
        bool update = false;
        string cellclicked = "";
        public Equipes()
        {
            InitializeComponent();
            InitGrid();
        }

        public async void InitGrid()
        {
            var teams = await new TeamsService().GetTeamsAsync(client);
            foreach (var team in teams.docs)
            {
                teamslist.Rows.Add(team.Id, team.Name, team.Logo, team.league.name, team.sport.ElementAt(0).Name);
            }
        }
        public void ResetGrid()
        {
            teamslist.Rows.Clear();
            InitGrid();
        }

        private void Equipes_Load(object sender, EventArgs e)
        {

        }

        private void add_Click(object sender, EventArgs e)
        {
            update = false;
            new Team_Actions(update).Show();
        }

        private void edit_Click(object sender, EventArgs e)
        {
            update = true;
            new Team_Actions(update, cellclicked).Show();
        }

        private void teamslist_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.teamslist.Rows[e.RowIndex];
                cellclicked = row.Cells["ID"].Value.ToString();
                string nameclicked = row.Cells["Libelle"].Value.ToString();

                MessageBox.Show("Vous avez sélectionné " + nameclicked);
            }
        }

        private async void delete_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Etes-vous sur de supprimer ?",
                                     "Suppression confirmée",
                                     MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                await new TeamsService().DeleteTeam(client, cellclicked);   
                MessageBox.Show("L'équipe a été supprimer avec success !!! ", "Suppression", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ResetGrid();
            }
        }

        private void Label20_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Match().Show();
        }

        private void Label19_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Users().Show();
        }

        private void Label18_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Equipes().Show();
        }

        private void Label17_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Article().Show();
        }

        private void Label16_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Dashboard().Show();
        }
    }
}
