﻿using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    partial class Users
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.league = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.equipe = new System.Windows.Forms.Label();
            this.Sport = new System.Windows.Forms.Label();
            this.matchs = new System.Windows.Forms.Label();
            this.userGrid = new System.Windows.Forms.DataGridView();
            this.username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nom = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsAdmin = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isEnable = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.logo = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(997, 87);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(833, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Deconnecter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(837, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Admnistrateur";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MidnightBlue;
            this.panel2.Controls.Add(this.league);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.equipe);
            this.panel2.Controls.Add(this.Sport);
            this.panel2.Controls.Add(this.matchs);
            this.panel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Location = new System.Drawing.Point(-9, 93);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(183, 491);
            this.panel2.TabIndex = 1;
            // 
            // league
            // 
            this.league.AutoSize = true;
            this.league.ForeColor = System.Drawing.SystemColors.Control;
            this.league.Location = new System.Drawing.Point(31, 174);
            this.league.Name = "league";
            this.league.Size = new System.Drawing.Size(43, 13);
            this.league.TabIndex = 1;
            this.league.Text = "League";
            this.league.Click += new System.EventHandler(this.league_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(31, 338);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Trésorerie";
            this.label6.Click += new System.EventHandler(this.Label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(31, 279);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Article";
            this.label5.Click += new System.EventHandler(this.article_Click);
            // 
            // equipe
            // 
            this.equipe.AutoSize = true;
            this.equipe.ForeColor = System.Drawing.SystemColors.Control;
            this.equipe.Location = new System.Drawing.Point(31, 224);
            this.equipe.Name = "equipe";
            this.equipe.Size = new System.Drawing.Size(40, 13);
            this.equipe.TabIndex = 0;
            this.equipe.Text = "Equipe";
            this.equipe.Click += new System.EventHandler(this.equipe_Click);
            // 
            // Sport
            // 
            this.Sport.AutoSize = true;
            this.Sport.ForeColor = System.Drawing.SystemColors.Control;
            this.Sport.Location = new System.Drawing.Point(31, 119);
            this.Sport.Name = "Sport";
            this.Sport.Size = new System.Drawing.Size(32, 13);
            this.Sport.TabIndex = 0;
            this.Sport.Text = "Sport";
            this.Sport.Click += new System.EventHandler(this.Sport_Click);
            // 
            // matchs
            // 
            this.matchs.AutoSize = true;
            this.matchs.ForeColor = System.Drawing.SystemColors.Control;
            this.matchs.Location = new System.Drawing.Point(31, 60);
            this.matchs.Name = "matchs";
            this.matchs.Size = new System.Drawing.Size(37, 13);
            this.matchs.TabIndex = 0;
            this.matchs.Text = "Match";
            this.matchs.Click += new System.EventHandler(this.matchs_Click);
            // 
            // userGrid
            // 
            this.userGrid.BackgroundColor = System.Drawing.Color.Navy;
            this.userGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.userGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.username,
            this.email,
            this.nom,
            this.Address,
            this.birthday,
            this.IsAdmin,
            this.isEnable});
            this.userGrid.Location = new System.Drawing.Point(226, 132);
            this.userGrid.Name = "userGrid";
            this.userGrid.Size = new System.Drawing.Size(763, 130);
            this.userGrid.TabIndex = 2;
            this.userGrid.Text = "dataGridView1";
            this.userGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.UserGrid_CellContentClick);
            // 
            // username
            // 
            this.username.HeaderText = "Username";
            this.username.Name = "username";
            // 
            // email
            // 
            this.email.HeaderText = "Email";
            this.email.Name = "email";
            // 
            // nom
            // 
            this.nom.HeaderText = "Name";
            this.nom.Name = "nom";
            // 
            // Address
            // 
            this.Address.HeaderText = "Adresse";
            this.Address.Name = "Address";
            // 
            // birthday
            // 
            this.birthday.HeaderText = "birthday";
            this.birthday.Name = "birthday";
            // 
            // IsAdmin
            // 
            this.IsAdmin.HeaderText = "IsAdmin";
            this.IsAdmin.Name = "IsAdmin";
            this.IsAdmin.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IsAdmin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // isEnable
            // 
            this.isEnable.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.isEnable.HeaderText = "isEnable";
            this.isEnable.Name = "isEnable";
            this.isEnable.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.isEnable.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // logo
            // 
            this.logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.logo.Image = global::FrontEnd_winform_pari.Properties.Resources.WannaBet;
            this.logo.InitialImage = null;
            this.logo.Location = new System.Drawing.Point(0, 0);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(174, 126);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 3;
            this.logo.TabStop = false;
            // 
            // Users
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 583);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.userGrid);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Users";
            this.Text = "Users";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.userGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label equipe;
        private System.Windows.Forms.Label Sport;
        private System.Windows.Forms.Label matchs;
        private System.Windows.Forms.DataGridView userGrid;
        private System.Windows.Forms.Label league;
        private System.Windows.Forms.DataGridViewTextBoxColumn username;
        private System.Windows.Forms.DataGridViewTextBoxColumn email;
        private System.Windows.Forms.DataGridViewTextBoxColumn nom;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthday;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IsAdmin;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isEnable;
        public void userGrid_CellValueChanged(object sender,
            DataGridViewCellEventArgs e)
        {
            MessageBox.Show("veillez verifier votre nom ou votre mot de passe svp", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            if (userGrid.Columns[e.ColumnIndex].Name == "isEnable")
            {
                MessageBox.Show("veillez verifier votre nom ou votre mot de passe svp", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private Button button1;
        private PictureBox logo;
    }
}