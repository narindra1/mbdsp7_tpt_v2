﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd_winform_pari.env
{
    public class api
    {
        public static string server = "https://tptnode.herokuapp.com/api/";
        public static string urlpath = "https://tptnode.herokuapp.com/static/image/";

        public static string getUrlImagePathStatic(string dir)
        {
            return urlpath + dir + "/";
        }
    }
}
