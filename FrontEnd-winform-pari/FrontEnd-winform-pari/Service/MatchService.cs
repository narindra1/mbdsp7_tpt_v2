﻿using FrontEnd_winform_pari.env;
using FrontEnd_winform_pari.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace FrontEnd_winform_pari.Service
{
    public class MatchService
    {
        private static string path = api.server;
        string fullpath = path + "matchs";
        string pathmvt = path+"matchs/mvtfinished";

        public async Task<ObjectPaginate<Matchs>> GetMatchsAsync(HttpClient client)
        {
            ObjectPaginate<Matchs> items = new ObjectPaginate<Matchs>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(fullpath)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;
                        //MessageBox.Show(resultString);

                        items = JsonConvert.DeserializeObject<ObjectPaginate<Matchs>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }
        public async Task<ObjectPaginate<Matchs>> GetMatchsDontAsync(HttpClient client)
        {
            ObjectPaginate<Matchs> items = new ObjectPaginate<Matchs>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(fullpath+ "/dontfinish")))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;
                        //MessageBox.Show(resultString);

                        items = JsonConvert.DeserializeObject<ObjectPaginate<Matchs>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }
        public async Task<ApiReturnMessage> InsertMatch(HttpClient client, Matchs matchs)
        {
            Random rd = new Random();
            matchs.Id = rd.Next(1000);

            var final = new
            {
                id = matchs.Id,
                id_team1 = matchs.team1_Id,
                id_team2 = matchs.team2_Id,
                date = matchs.Date,
                time = matchs.Time,
                quote_team1 = matchs.quote_1,
                quote_team2 = matchs.quote_2,
                quote_null = matchs.quote_null,
            };
            string json = JsonConvert.SerializeObject(final);
            StringContent data = new StringContent(json, Encoding.UTF8, "application/json");
            ApiReturnMessage items = new ApiReturnMessage();
            try
            {
               

                    using (var response = await client.PostAsync(new Uri(fullpath), data))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            using (HttpContent contentResponse = response.Content)
                            {
                                string resultString = await contentResponse.ReadAsStringAsync();
                                string reasonPhrase = response.ReasonPhrase;
                                HttpResponseHeaders headers = response.Headers;
                                HttpStatusCode code = response.StatusCode;

                                items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Un problème est survenu lors de l'insertion", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }
        public async Task<ApiReturnMessage> FinalizMatch(HttpClient client, int id,int score_1,int score_2,int idteamwinner)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            try
            {
                var final = new
                {
                    idmatch = id,
                    score_1 = score_1,
                    score_2 = score_2,
                    idteamwinner = idteamwinner,
                };
                string json = JsonConvert.SerializeObject(final);
                StringContent data = new StringContent(json, Encoding.UTF8, "application/json");
                using (var response = await client.PutAsync(new Uri(pathmvt), data))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            using (HttpContent contentResponse = response.Content)
                            {
                                string resultString = await contentResponse.ReadAsStringAsync();
                                string reasonPhrase = response.ReasonPhrase;
                                HttpResponseHeaders headers = response.Headers;
                                HttpStatusCode code = response.StatusCode;

                                items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Un problème est survenu lors de l'insertion", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<ApiReturnMessage> DeleteMatchAsync(string id, HttpClient client)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            string pathUri = fullpath + "/" + id;
            try
            {
                using (HttpResponseMessage response = await client.DeleteAsync(new Uri(pathUri)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<ApiReturnMessage> UpdateMatch(HttpClient client, Matchs matchs)
        {
            MessageBox.Show("team 1 " + matchs.team1_Id + " team 2 " + matchs.team2_Id + " date " + matchs.Date_time + " time " + matchs.Time);
            ApiReturnMessage items = new ApiReturnMessage();
            try
            {
                var json = JsonConvert.SerializeObject(matchs);
                var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                using (var response = await client.PutAsync(new Uri(fullpath), content))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        using (HttpContent contentResponse = response.Content)
                        {
                            string resultString = await contentResponse.ReadAsStringAsync();
                            string reasonPhrase = response.ReasonPhrase;
                            HttpResponseHeaders headers = response.Headers;
                            HttpStatusCode code = response.StatusCode;

                            items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                            MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Un problème est survenu lors de la mise à jour du match", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<List<Matchs>> getOneMatchAsync(HttpClient client, string id)
        {
            List<Matchs> items = new List<Matchs>();
            string pathUri = fullpath + "/" + id;
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(pathUri)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;
                        items = JsonConvert.DeserializeObject<List<Matchs>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

    }
}
