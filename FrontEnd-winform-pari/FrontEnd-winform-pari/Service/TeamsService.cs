﻿using FrontEnd_winform_pari.env;
using FrontEnd_winform_pari.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari.Service
{
    public class TeamsService
    {
        private static string path = api.server;
        string fullpath = path + "Teams";
        public async Task<ObjectPaginate<Teams>> GetTeamsAsync(HttpClient client)
        {
            ObjectPaginate<Teams> items = new ObjectPaginate<Teams>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(fullpath)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;
                        //MessageBox.Show(resultString);

                        items = JsonConvert.DeserializeObject<ObjectPaginate<Teams>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<List<Teams>> getOneTeamAsync(HttpClient client, string id)
        {
            List<Teams> items = new List<Teams>();
            string pathUri = fullpath + "/" + id;
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(pathUri)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        //MessageBox.Show(resultString);

                        items = JsonConvert.DeserializeObject<List<Teams>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<ApiReturnMessage> InserTeam(HttpClient client, Teams teams, byte[] image, string filename)
        {

            ApiReturnMessage items = new ApiReturnMessage();
            try
            {
                using (var content = new MultipartFormDataContent())
                {
                    Random rd = new Random();
                    content.Headers.ContentType.MediaType = "multipart/form-data";
                    content.Add(new StreamContent(new MemoryStream(image)), "file", filename);
                    content.Add(new StringContent(rd.Next(1000).ToString()), "id");
                    content.Add(new StringContent(teams.id_league.ToString()), "id_league");
                    content.Add(new StringContent(teams.Name.ToString()), "name");

                    using (var response = await client.PostAsync(new Uri(fullpath), content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            using (HttpContent contentResponse = response.Content)
                            {
                                string resultString = await contentResponse.ReadAsStringAsync();
                                string reasonPhrase = response.ReasonPhrase;
                                HttpResponseHeaders headers = response.Headers;
                                HttpStatusCode code = response.StatusCode;

                                items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Un problème est survenu lors de l'insertion", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<ApiReturnMessage> DeleteTeam(HttpClient client, string id)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            string pathUri = fullpath + "/" + id;
            try
            {
                using (HttpResponseMessage response = await client.DeleteAsync(new Uri(pathUri)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<ApiReturnMessage> UpdateTeam(HttpClient client, Teams teams, byte[] image, string filename, string imageolder)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            try
            {
                if (image != null)
                {
                    MessageBox.Show("entre dans image != null");
                    string pathUri = fullpath + "/file";
                    using (var content = new MultipartFormDataContent())
                    {
                        content.Headers.ContentType.MediaType = "multipart/form-data";
                        content.Add(new StringContent(teams.Id.ToString()), "id");
                        content.Add(new StringContent(teams.Name.ToString()), "name");
                        content.Add(new StreamContent(new MemoryStream(image)), "file", filename);
                        content.Add(new StringContent(teams.id_league.ToString()), "id_league");
                        content.Add(new StringContent(imageolder), "image");

                        using (HttpResponseMessage response = await client.PostAsync(new Uri(pathUri), content))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent contentResponse = response.Content)
                                {
                                    string resultString = await contentResponse.ReadAsStringAsync();
                                    string reasonPhrase = response.ReasonPhrase;
                                    HttpResponseHeaders headers = response.Headers;
                                    HttpStatusCode code = response.StatusCode;

                                    items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                    MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Un problème est survenu lors de la mise à jour", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("entre dans image == null");
                    var postData = new Dictionary<string, string>();
                    postData.Add("id", teams.Id.ToString());
                    postData.Add("id_league", teams.id_league.ToString());
                    postData.Add("name", teams.Name);
                    postData.Add("logo", teams.Logo);

                    using (var content = new FormUrlEncodedContent(postData))
                    {
                        content.Headers.Clear();
                        content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                        using (HttpResponseMessage response = await client.PutAsync(new Uri(fullpath), content))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent contentResponse = response.Content)
                                {
                                    string resultString = await contentResponse.ReadAsStringAsync();
                                    string reasonPhrase = response.ReasonPhrase;
                                    HttpResponseHeaders headers = response.Headers;
                                    HttpStatusCode code = response.StatusCode;

                                    items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                    MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Un problème est survenu lors de la mise à jour", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

    }
}
