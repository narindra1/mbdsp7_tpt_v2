using FrontEnd_winform_pari.env;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using FrontEnd_winform_pari.Model;
using System.Windows.Forms;
using System.IO;

namespace FrontEnd_winform_pari.Service
{
    public class LeagueService
    {
        private static string path = api.server;
        string fullpath = path + "Leagues";
        public async Task<ObjectPaginate<League>> GetLeaguesAsync(HttpClient client)
        {
            ObjectPaginate<League> items = new ObjectPaginate<League>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(fullpath)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        //MessageBox.Show(resultString);

                        items = JsonConvert.DeserializeObject<ObjectPaginate<League>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<List<League>> getOneLeagueAsync(HttpClient client, string id)
        {
            List<League> items = new List<League>();
            string pathUri = fullpath + "/" + id;
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(pathUri)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        //MessageBox.Show(resultString);

                        items = JsonConvert.DeserializeObject<List<League>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<ApiReturnMessage> DeleteLeague(HttpClient client, string id)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            string pathUri = fullpath + "/" + id;
            try
            {
                using (HttpResponseMessage response = await client.DeleteAsync(new Uri(pathUri)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<ApiReturnMessage> InsertLeague(HttpClient client, League league, byte[] image, string filename)
        {

            ApiReturnMessage items = new ApiReturnMessage();
            try
            {
                using (var content = new MultipartFormDataContent())
                {
                    Random rd = new Random();
                    MessageBox.Show("filename = " + filename);
                    content.Headers.ContentType.MediaType = "multipart/form-data";
                    content.Add(new StreamContent(new MemoryStream(image)), "file", filename);
                    content.Add(new StringContent(rd.Next(1000).ToString()), "id");
                    content.Add(new StringContent(league.id_sport.ToString()), "id_sport");
                    content.Add(new StringContent(league.name.ToString()), "name");

                    using (var response = await client.PostAsync(new Uri(fullpath), content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            using (HttpContent contentResponse = response.Content)
                            {
                                string resultString = await contentResponse.ReadAsStringAsync();
                                string reasonPhrase = response.ReasonPhrase;
                                HttpResponseHeaders headers = response.Headers;
                                HttpStatusCode code = response.StatusCode;

                                items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Un problème est survenu lors de l'insertion", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }
        public async Task<ApiReturnMessage> updateLeague(HttpClient client, League league, byte[] image, string filename, string imageolder)
        {
            ApiReturnMessage items = new ApiReturnMessage();
     
            try
            {
                if (image != null)
                {
                    Console.WriteLine("updateLeague -> image is != null");
                    string pathUri = fullpath + "/file";
                    using (var content = new MultipartFormDataContent())
                    {
                        Random rd = new Random();
                        content.Add(new StreamContent(new MemoryStream(image)), "file", filename);
                        content.Add(new StringContent(league.id.ToString()), "id");
                        content.Add(new StringContent(league.id_sport.ToString()), "id_sport");
                        content.Add(new StringContent(league.name.ToString()), "name");
                        content.Add(new StringContent(imageolder), "image");
                        //Console.WriteLine(" league.id = " + league.id);
                        //Console.WriteLine(" league.name = " + league.name);
                        //Console.WriteLine(" league.id_sport = " + league.id_sport);
                        //Console.WriteLine(" league.image = " + league.image);
                        //Console.WriteLine(" pathUri = " + pathUri);
                        //MessageBox.Show("filename = " + filename);

                        using (HttpResponseMessage response = await client.PostAsync(new Uri(pathUri), content))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent contentResponse = response.Content)
                                {
                                    string resultString = await contentResponse.ReadAsStringAsync();
                                    string reasonPhrase = response.ReasonPhrase;
                                    HttpResponseHeaders headers = response.Headers;
                                    HttpStatusCode code = response.StatusCode;

                                    items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                    MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Un problème est survenu lors de la mise à jour", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine("updateLeague -> image is == null");
                    var postData = new Dictionary<string, string>();
                    postData.Add("id", league.id.ToString());
                    postData.Add("id_sport", league.id_sport.ToString());
                    postData.Add("name", league.name);
                    postData.Add("image", league.image);

                    using (var content = new FormUrlEncodedContent(postData))
                    {
                        content.Headers.Clear();
                        content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                        using (HttpResponseMessage response = await client.PutAsync(new Uri(fullpath), content))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent contentResponse = response.Content)
                                {
                                    string resultString = await contentResponse.ReadAsStringAsync();
                                    string reasonPhrase = response.ReasonPhrase;
                                    HttpResponseHeaders headers = response.Headers;
                                    HttpStatusCode code = response.StatusCode;

                                    items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                    MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Un problème est survenu lors de la mise à jour", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }
    }
}

