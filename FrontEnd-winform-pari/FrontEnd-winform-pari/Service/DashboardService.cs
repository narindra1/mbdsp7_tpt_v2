﻿using FrontEnd_winform_pari.env;
using FrontEnd_winform_pari.Model;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari.Service
{
    class DashboardService
    {
        private static string path = api.server;
        string fullpath = path + "solde/site";
        string mstbet = path + "match/mostbet";
        string bet = path + "paris";
        String team = path+"pari/statistic";
        String mois = path+"pari/mois/";
        public async Task<Dashboards> GetDashboardAsync(HttpClient client)
        {
            Dashboards items = new Dashboards();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(fullpath)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<Dashboards>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
              //  MessageBox.Show("veillez verifier votre nom ou votre mot de passe svp" + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
            return items;
        }
        public async Task<List<MatchChart>> ChartMatch(HttpClient client)
        {
            List<MatchChart> items = new List<MatchChart>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(mstbet)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<List<MatchChart>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show("veillez verifier votre nom ou votre mot de passe svp" + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
            return items;
        }
        public async Task<List<TeamChart>> ChartTeam(HttpClient client)
        {
            List<TeamChart> items = new List<TeamChart>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(team)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<List<TeamChart>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show("veillez verifier votre nom ou votre mot de passe svp" + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
            return items;
        }
        public async Task<List<List<Object>>> ChartParMois(HttpClient client,int year)
        {
            mois += year;
            List<List<Object>> items = new List<List<Object>>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(mois)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<List<List<Object>>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show("veillez verifier votre nom ou votre mot de passe svp" + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
            return items;
        }

        public async Task<int> GetTotalBetAsync(HttpClient client)
        {
            int reponse = 0;
            ArrayList items = new ArrayList();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(bet)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<ArrayList>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                //  MessageBox.Show("veillez verifier votre nom ou votre mot de passe svp" + ex, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
            reponse = items.Count;
            return reponse;
        }

    }
}
