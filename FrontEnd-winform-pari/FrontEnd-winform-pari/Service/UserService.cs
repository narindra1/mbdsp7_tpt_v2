﻿using FrontEnd_winform_pari.env;
using FrontEnd_winform_pari.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari.Service
{
    class UserService
    {
        private static string path = api.server;
        string fullpath = path + "user";
        public async Task<ApiReturnMessage> Login(Form login,HttpClient client, String username,String password)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            try
            {

                using (var content = new MultipartFormDataContent())
                {
                    User user = new User();
                    user.username = username;
                    user.password = password;
                    var stringContent = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
                    using (var response = await client.PostAsync(new Uri(fullpath+"/loginAdmin"), stringContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            using (HttpContent contentResponse = response.Content)
                            {
                                string resultString = await contentResponse.ReadAsStringAsync();
                                string reasonPhrase = response.ReasonPhrase;
                                HttpResponseHeaders headers = response.Headers;
                                HttpStatusCode code = response.StatusCode;

                                items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                MessageBox.Show("Bienvenue "+ username, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                new Users().Show();
                                login.Hide();
                            }
                        }
                        else
                        {
                            MessageBox.Show("veillez verifier votre nom ou votre mot de passe svp", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }
        public async Task<ObjectPaginate<User>> GetUsers(HttpClient client)
        {
            ObjectPaginate<User> items = new ObjectPaginate<User>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(fullpath)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;
                        //MessageBox.Show(resultString);

                        items = JsonConvert.DeserializeObject<ObjectPaginate<User>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

    }
}
