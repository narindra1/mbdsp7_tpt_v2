﻿using FrontEnd_winform_pari.env;
using FrontEnd_winform_pari.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari.Service
{
    public class SportsService
    {
        private static string path = api.server;
        string fullpath = path + "sports";
        public async Task<ObjectPaginate<Sports>> GetSportsAsync(HttpClient client)
        {
            ObjectPaginate<Sports> items = new ObjectPaginate<Sports>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(fullpath)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;
                        //MessageBox.Show(resultString);

                        items = JsonConvert.DeserializeObject<ObjectPaginate<Sports>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<Sports> getOneSportAsync(HttpClient client, string id)
        {
            Sports items = new Sports();
            string pathUri = fullpath + "/" + id;
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(pathUri)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;
                        items = JsonConvert.DeserializeObject<Sports>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<ApiReturnMessage> InsertSport(HttpClient client, Sports sports, byte[] image, string filename)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            string pathUri = fullpath;
            try
            {
                using (var content = new MultipartFormDataContent())
                {
                    content.Headers.ContentType.MediaType = "multipart/form-data";
                    content.Add(new StreamContent(new MemoryStream(image)), "file", filename);
                    Random rd = new Random();
                    content.Add(new StringContent(rd.Next(1000).ToString()), "id");
                    content.Add(new StringContent(sports.Name), "name");

                    using (var response = await client.PostAsync(new Uri(fullpath), content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            using (HttpContent contentResponse = response.Content)
                            {
                                string resultString = await contentResponse.ReadAsStringAsync();
                                string reasonPhrase = response.ReasonPhrase;
                                HttpResponseHeaders headers = response.Headers;
                                HttpStatusCode code = response.StatusCode;

                                items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Un problème est survenu lors de l'insertion", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        public async Task<ApiReturnMessage> UpdateSport(HttpClient client, Sports sports, byte[] image, string filename, string imageolder)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            try
            {
                if (image != null)
                {
                    MessageBox.Show("entre dans image != null");
                    string pathUri = fullpath + "/file";
                    using (var content = new MultipartFormDataContent())
                    {
                        content.Headers.ContentType.MediaType = "multipart/form-data";
                        content.Add(new StreamContent(new MemoryStream(image)), "file", filename);
                        content.Add(new StringContent(sports.Id.ToString()), "id");
                        content.Add(new StringContent(sports.Name), "name");
                        content.Add(new StringContent(imageolder), "image");

                        using (HttpResponseMessage response = await client.PostAsync(new Uri(pathUri), content))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent contentResponse = response.Content)
                                {
                                    string resultString = await contentResponse.ReadAsStringAsync();
                                    string reasonPhrase = response.ReasonPhrase;
                                    HttpResponseHeaders headers = response.Headers;
                                    HttpStatusCode code = response.StatusCode;

                                    items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                    MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Un problème est survenu lors de la mise à jour", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("entre dans image == null");
                    var postData = new Dictionary<string, string>();
                    postData.Add("id", sports.Id.ToString());
                    postData.Add("name", sports.Name);
                    postData.Add("image", sports.image);

                    using (var content = new FormUrlEncodedContent(postData))
                    {
                        content.Headers.Clear();
                        content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                        using (HttpResponseMessage response = await client.PutAsync(new Uri(fullpath), content))
                        {
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent contentResponse = response.Content)
                                {
                                    string resultString = await contentResponse.ReadAsStringAsync();
                                    string reasonPhrase = response.ReasonPhrase;
                                    HttpResponseHeaders headers = response.Headers;
                                    HttpStatusCode code = response.StatusCode;

                                    items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                    MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                            else
                            {
                                MessageBox.Show("Un problème est survenu lors de la mise à jour", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }


        public async Task<ApiReturnMessage> DeleteSportsAsync(string id, HttpClient client)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            string pathUri = fullpath + "/" + id;
            try
            {
                using (HttpResponseMessage response = await client.DeleteAsync(new Uri(pathUri)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }
    }
}
