﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari.Service
{
    public class GlobalService
    {
        public (string, byte[]) ChooseFileDialog(PictureBox pictureBox, TextBox textBox)
        {
            string chemin = "";
            byte[] file = null;

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.InitialDirectory = "Images";
            dialog.Title = "Choisir une fichier à importer";
            dialog.Filter = "Select Valid Document(*.jpg; *.jpeg; *.png; *.tif; *.tiff; *.gif)|*.jpg; *.jpeg; *.png; *.tif; *.tiff; *.gif";
            dialog.FilterIndex = 1;
            try
            {
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    if (dialog.CheckFileExists)
                    {
                        string fichier = Path.GetFileName(dialog.FileName);
                        chemin = Path.GetFullPath(dialog.FileName);
                        textBox.Text = fichier;
                        file = File.ReadAllBytes(dialog.FileName);
                        Bitmap bitmap = new Bitmap(dialog.FileName);
                        pictureBox.Image = bitmap;
                        pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
                    }
                }
                else
                {
                    MessageBox.Show("Veuillez choisir un fichier");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return (chemin, file);
        }
    }
}
