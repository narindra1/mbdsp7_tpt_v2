﻿using FrontEnd_winform_pari.env;
using FrontEnd_winform_pari.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari.Service
{
    class ArticleService
    {
        private static string path = api.server;
        string fullpath = path + "articles";
        public async Task<ObjectPaginate<Articles>> GetArticlesAsync(HttpClient client)
        {
            ObjectPaginate<Articles> items = new ObjectPaginate<Articles>();
            try
            {
                using (HttpResponseMessage response = await client.GetAsync(new Uri(fullpath)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;


                        items = JsonConvert.DeserializeObject<ObjectPaginate<Articles>>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }
        public async Task<ApiReturnMessage> InsertArticle(HttpClient client, Articles article, byte[] image, string filename )
        {
            ApiReturnMessage items = new ApiReturnMessage();
            string pathUri = fullpath;
            try
            {
                using (var content = new MultipartFormDataContent())
                {
                    content.Headers.ContentType.MediaType = "multipart/form-data";
                    content.Add(new StreamContent(new MemoryStream(image)), "file", filename);
                    Random rd = new Random();
                    content.Add(new StringContent(rd.Next(1000).ToString()), "id");
                    content.Add(new StringContent(article.titre), "titre");
                    content.Add(new StringContent(article.date.ToString()), "date");
                    content.Add(new StringContent(article.description), "description");

                    using (var response = await client.PostAsync(new Uri(fullpath), content))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            using (HttpContent contentResponse = response.Content)
                            {
                                string resultString = await contentResponse.ReadAsStringAsync();
                                string reasonPhrase = response.ReasonPhrase;
                                HttpResponseHeaders headers = response.Headers;
                                HttpStatusCode code = response.StatusCode;

                                items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                                MessageBox.Show(items.message, "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Un problème est survenu lors de l'insertion", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

        internal Task DeleteArticleAsync(object cellclicked, HttpClient client)
        {
            throw new NotImplementedException();
        }

        public async Task<ApiReturnMessage> DeleteArticleAsync(string id, HttpClient client)
        {
            ApiReturnMessage items = new ApiReturnMessage();
            string pathUri = fullpath + "/" + id;
            try
            {
                using (HttpResponseMessage response = await client.DeleteAsync(new Uri(pathUri)))
                {
                    using (HttpContent content = response.Content)
                    {
                        string resultString = await content.ReadAsStringAsync();
                        string reasonPhrase = response.ReasonPhrase;
                        HttpResponseHeaders headers = response.Headers;
                        HttpStatusCode code = response.StatusCode;

                        items = JsonConvert.DeserializeObject<ApiReturnMessage>(resultString);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return items;
        }

    }
}
