﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            BarExample();
        }
        public void BarExample()
        {
            chart1.Series["Match"].Points.AddXY("Ajay", "10000");
            chart1.Series["Match"].Points.AddXY("Ramesh", "8000");
            chart1.Series["Match"].Points.AddXY("Ankit", "7000");
            chart1.Series["Match"].Points.AddXY("Gurmeet", "10000");
            chart1.Series["Match"].Points.AddXY("Suresh", "8500");
            //chart title  
            chart1.Titles.Add("Salary Chart");
        }
    }
}
