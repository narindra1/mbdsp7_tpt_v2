﻿using FrontEnd_winform_pari.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrontEnd_winform_pari
{
    public partial class Leagues : Form
    {
        static HttpClient client = new HttpClient();
        bool update = false;
        string cellclicked = "";
        public Leagues()
        {
            InitializeComponent();
            InitGrid();
        }

        public async void InitGrid()
        {
            var leagues = await new LeagueService().GetLeaguesAsync(client);
            foreach (var league in leagues.docs)
            {
                leaguelist.Rows.Add(league.id, league.sport.ElementAt(0).Name, league.image, league.name);
            }
        }

        public void ResetGrid()
        {
            leaguelist.Rows.Clear();
            InitGrid();
        }

        private void Leagues_Load(object sender, EventArgs e)
        {

        }

        private void add_Click(object sender, EventArgs e)
        {
            update = false;
            new League_Actions(update).Show();
        }

        private void leaguelist_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow row = this.leaguelist.Rows[e.RowIndex];
                cellclicked = row.Cells["ID"].Value.ToString();
                string nameclicked = row.Cells["League"].Value.ToString();

                MessageBox.Show("Vous avez sélectionné " + nameclicked);
            }
        }

        private void edit_Click(object sender, EventArgs e)
        {
            update = true;
            new League_Actions(update, cellclicked).Show();
        }

        private async void delete_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Etes-vous sur de supprimer ?",
                                      "Suppression confirmée",
                                      MessageBoxButtons.YesNo);
            if (confirmResult == DialogResult.Yes)
            {
                await new LeagueService().DeleteLeague(client, cellclicked);
                MessageBox.Show("La ligue a été supprimer avec success !!! ", "Suppression", MessageBoxButtons.OK, MessageBoxIcon.Information);

                ResetGrid();
            }
        }

        private void Label20_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Match().Show();
        }

        private void Label19_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Users().Show();
        }

        private void Label18_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Equipes().Show();
        }

        private void Label17_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Article().Show();
        }

        private void Label16_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Dashboard().Show();
        }
    }
}
