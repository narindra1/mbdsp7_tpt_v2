
export class User {
    _id?:string;
    id:number;
    email:string;
    name:string;
    username:string;
    address:number;
    birthday:Date;
    password:String
    isAdmin:Boolean
    isEnable:Boolean
  }
  