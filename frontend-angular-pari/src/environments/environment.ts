// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBirvwozyTpYlQDALVUDdkN8A4WFY5kDrY",
    authDomain: "wannabet-1aaf4.firebaseapp.com",
    projectId: "wannabet-1aaf4",
    storageBucket: "wannabet-1aaf4.appspot.com",
    messagingSenderId: "1070579777731",
    appId: "1:1070579777731:web:b685d1bd1bf527c1f3a276"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
